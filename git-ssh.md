### [RETURN TO README](README.md#markdown-header-table-of-contents)
---

 &nbsp; &nbsp;




### **Table of Contents**
- [Git Globals](#markdown-header-git-globals) | *Setup git global config*
- [Generate SSH Keys](#markdown-header-ssh-keys) | *Generate hardened ssh keys per machine*
- [Git Repo Remote](#markdown-header-git-repo-remote) | *How to clone, add, update, and view repo urls*
- [SSH Default Settings](#markdown-header-ssh-default-settings) | *Setup Default Settings for your ssh keys*
- [SSH Entry Example](#markdown-header-ssh-entry-example) | *SSH Config File Entry Definition*
- [Git Repo Commands](#markdown-header-git-repo-commands) | *Cheatsheet for git commands*

 &nbsp; &nbsp;




### **Git Globals**
*Setup git global config*
```sh
git config --global user.name "****"
git config --global user.email "****"
git config --global credential.helper cache
git config --global credential.helper 'cache --timeout=3600'
git config --global init.defaultBranch main
git config --global pull.rebase false
git config --global --add safe.directory "*"
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **SSH Keys**
*Generate hardened ssh keys per machine*
```sh
# Master Keys
rm /etc/ssh/ssh_host_ecdsa_key*
rm /etc/ssh/ssh_host_ed25519_key*
rm /etc/ssh/ssh_host_rsa_key*
ssh-keygen -a 500 -t ecdsa -b 521 -N "" -C "" -f /etc/ssh/ssh_host_ecdsa_key
ssh-keygen -a 500 -t ed25519 -N "" -C "" -f /etc/ssh/ssh_host_ed25519_key
ssh-keygen -a 500 -t rsa -b 8192 -N "" -C "" -f /etc/ssh/ssh_host_rsa_key

# Definition
ssh-keygen -a 500 -t ed25519 -N "" -C "" -f ~/.ssh/{filename}

# Standard Keys
ssh-keygen -a 500 -t ed25519 -N "" -C "" -f ~/.ssh/bitbucket
ssh-keygen -a 500 -t ed25519 -N "" -C "" -f ~/.ssh/github
ssh-keygen -a 500 -t ed25519 -N "" -C "" -f ~/.ssh/gitlab

# Update Permissions
sudo chmod 400 ~/.ssh/*
sudo chmod 600 ~/.ssh/config ~/.ssh/known_hosts*
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Git Repo Remote**
*How to clone, add, update, and view repo urls*
```sh
# Clone New Repo
git clone {tag}:{account}/{repository}
git clone {user}@{url}:{account}/{repository}

# Add Repo Remote Location
git remote add origin {tag}:{account}/{repository}
git remote add origin {user}@{url}:{account}/{repository}

# Update Repo Remote Location
git remote set-url origin {tag}:{account}/{repository}
git remote set-url origin {user}@{url}:{account}/{repository}

# View Repo Remote Location
git remote -v

# Examples
git clone github:{account}/{repository}
git clone git@github.com:{account}/{repository}

git clone bitbucket:{account}/{repository}
git clone git@bitbucket.org:{account}/{repository}
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **SSH Default Settings**
*Setup Default Settings for your ssh keys*
```sh
# Entry Definition
Host {tag|url}
	HostName {url}
	User {user}
	IdentitiesOnly yes
	IdentityFile ~/.ssh/{filename}
	HostKeyAlgorithms ssh-ed25519,ssh-rsa

# Create or replace ssh config
sudo tee ~/.ssh/config > /dev/null <<EOF
# ----- Default Common ------------------------------------------------------------

Host bitbucket *bitbucket.org
    Hostname bitbucket.org
    IdentityFile ~/.ssh/bitbucket
    HostKeyAlgorithms ssh-ed25519,ssh-rsa
    PubkeyAcceptedAlgorithms ssh-ed25519,ssh-rsa
#    PubkeyAcceptedKeyTypes ssh-ed25519,ssh-rsa

Host github *github.com
    HostName github.com
    IdentityFile ~/.ssh/github

Host gitlab *gitlab.com
    Hostname gitlab.com
    IdentityFile ~/.ssh/gitlab

# ----- Default Global ------------------------------------------------------------

Host *
    User git
    AddKeysToAgent no
    ServerAliveCountMax 40
    ServerAliveInterval 15
    IdentitiesOnly yes
    HostKeyAlgorithms ssh-ed25519
    PreferredAuthentications publickey
    PubkeyAcceptedAlgorithms ssh-ed25519
#    PubkeyAcceptedKeyTypes ssh-ed25519,ssh-rsa
    StrictHostKeyChecking accept-new

EOF

# Update Permissions
sudo chmod 400 ~/.ssh/*
sudo chmod 600 ~/.ssh/config ~/.ssh/known_hosts* 2>/dev/null
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **SSH Entry Example**
*SSH Config File Entry Definition*
```sh
# Definition Example
Host bb2
	HostName bitbucket.org
	User bot
	IdentitiesOnly yes
	IdentityFile ~/.ssh/example

# Example - Server Login
ssh bb2
ssh smith@bb2

# SSH Force Password
ssh -o PubkeyAuthentication=no -o PreferredAuthentications=password user@server

# Example - Repo Clone
git clone bb2:my_account/my_repo
git clone smith@bb2:my_account/my_repo
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Git Repo Commands**
*Cheatsheet for git commands*
```sh
# See if any files changes
git status

# Add all changed files to commit
git add -A

# Commit and comment all changes
git commit -m "changes"

# Pull in new changes
git pull

# Push to and track branch master from origin
git push -u origin master

# Combined Shortcut
git add -A && git commit -m "changes" && git pull && git push -u origin master

# Discard all changes
git reset HEAD --hard

# Change to another branch
git checkout {branch_name}
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;
