#!/usr/bin/env bash

CWD=$(pwd);

DIR=~/Scripts/gnome-ext-src/dash-to-dock
if [ ! -d $DIR ]; then
	mkdir -p $DIR
	git clone https://github.com/micheleg/dash-to-dock ./
fi
cd $DIR
git pull
make clean
make _build
UUID=$(cat _build/metadata.json | jq -r '.uuid')
sudo rm -rf /usr/share/gnome-shell/extensions/$UUID
sudo mv _build /usr/share/gnome-shell/extensions/$UUID
cd $CWD


DIR=~/Scripts/gnome-ext-src/gTile
if [ ! -d $DIR ]; then
	mkdir -p $DIR
	git clone https://github.com/gTile/gTile ./
fi
cd $DIR
git pull
npm run clean
npm ci
npm run build:dist
npm run install:extension
UUID=$(cat dist/metadata.json | jq -r '.uuid')
sudo rm -rf /usr/share/gnome-shell/extensions/$UUID
sudo mv ~/.local/share/gnome-shell/extensions/$UUID /usr/share/gnome-shell/extensions/$UUID
cd $CWD


DIR=~/Scripts/gnome-ext-src/Vitals/
if [ ! -d $DIR ]; then
	mkdir -p $DIR
	git clone https://github.com/corecoding/Vitals ./
fi
cd $DIR
git pull
UUID=$(cat metadata.json | jq -r '.uuid')
sudo rm -rf /usr/share/gnome-shell/extensions/$UUID
sudo cp -R ./ /usr/share/gnome-shell/extensions/$UUID
cd $CWD
