### [RETURN TO README](README.md#markdown-header-table-of-contents)
---

 &nbsp; &nbsp;




### **Table of Contents**
- [Mount ISO - Grub Config](#markdown-header-mount-iso-grub-config) | *Grub Config To Mount Arch Linux ISO*
- [Wifi & Lan Connect](#markdown-header-wifi-lan-connect) | *Commands to Manually connect to wifi or lan for internet access*
- [Secure Wipe](#markdown-header-secure-wipe) | *Commands To Delete Data from Disk and no recovery*
- [OS Partition Backup](#markdown-header-os-partition-backup) | *Commands To Backup OS Partition Table in case of accidental overwrite*
- [Disk Partition](#markdown-header-disk-partition) | *Commands To Partition Disk*
- [Format & Mount Drives](#markdown-header-format-mount-drives) | *Commands To Format Partitions*
- [Swap File](#markdown-header-swap-file) | *Instructions to create a swap file instead of a swap partition*
- [Package Repo Setup](#markdown-header-package-repo-setup) | *Commands to generate Arch Linux Package Configuration*
- [OS Package Install](#markdown-header-os-package-install) | *List of commonly used packages for Arch Linux*
- [Fstab Example](#markdown-header-fstab-example) | *List of commonly used packages for Arch Linux*
- [System Configuration](#markdown-header-system-configuration) | *Update system files for Arch Linux to function properly*
- [Grub Install](#markdown-header-grub-install) | *Instructions to install a fresh Grub Instance*
- [Grub Update](#markdown-header-grub-update) | *Instructions to update Grub to include new OS*
- [Configure Users](#markdown-header-configure-users) | *Commands to create, modify, and delete user accounts*
- [Resolve Keyring Issues](#markdown-header-resolve-keyring-issues) | *How to fix Keyring Issues*
- [Gnome Extensions](#markdown-header-gnome-extensions) | *List of useful gnome extensions*
- [AUR Package Install](#markdown-header-aur-package-install) | *List of useful aur packages*
- [Title](xxxxxxx) | *Description*

 &nbsp; &nbsp;




### **Mount ISO - Grub Config**

*Grub Config To Mount Arch Linux ISO*

```sh
## Find Disk UUID
lsblk
blkid | sort

## Grub Menu Entry
## /etc/grub.d/40_custom
set grub_get_arch="YYYY.MM.DD"
set grub_get_uuid="XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX"

menuentry "ISO: Arch Linux ${grub_get_arch}" {
	insmod fat
	insmod part_gpt
	set get_iso="/iso/archlinux-${grub_get_arch}-x86_64.iso"
	set get_disk="/dev/disk/by-uuid/${grub_get_uuid}"
	search --no-floppy --fs-uuid --set=root "$grub_get_uuid"
	loopback loop $get_iso
	linux (loop)/arch/boot/x86_64/vmlinuz-linux root=UUID="$grub_get_uuid" img_dev="$get_disk" img_loop="$get_iso" earlymodules=loop
	initrd (loop)/arch/boot/intel-ucode.img (loop)/arch/boot/amd-ucode.img (loop)/arch/boot/x86_64/initramfs-linux.img
}

menuentry "System Restart" {
	reboot
}

menuentry "System Poweroff" {
	poweroff
}

## Notes
## - Global Variables must start with "set grub_get_"
## - Local Variables must start with "set get_"
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Wifi & Lan Connect**

*Commands to Manually connect to wifi or lan for internet access*

```sh
## Define Wifi Connection
WIFI_SSID=
WIFI_PASS=
WIFI_SSID_2G=
WIFI_PASS_2G=

## Check Connection
ip -4 -br a

## List Network Connectors
networkctl list --no-pager

## Wifi using iwctl
ip link set wlan0 up
iwctl station wlan0 scan
iwctl station wlan0 get-networks
iwctl station wlan0 connect $WIFI_SSID --passphrase $WIFI_PASS

## Wifi using iwctl files
ip link set wlan0 up
cat > /var/lib/iwd/${WIFI_SSID}.psk <<EOF
[Security]
PreSharedKey=$(wpa_passphrase "$WIFI_SSID" "$WIFI_PASS" | grep -v '#psk' | grep psk | cut -d'=' -f2)
Passphrase=$WIFI_PASS
EOF

## Wifi using wpa_supplicant
ip link set wlan0 up
iw dev wlan0 scan | grep SSID: | sort -Vu
wpa_supplicant -B -i wlan0 -c <(wpa_passphrase MYSSID passphrase)

## Wifi using wpa_supplicant files
ip link set wlan0 up
iw dev wlan0 scan | grep SSID: | sort -Vu
cat > /etc/wpa_supplicant/wpa_supplicant-wlan0.conf <<EOF
ctrl_interface=/var/run/wpa_supplicant
ctrl_interface_group=0
update_config=0

EOF
wpa_passphrase "$WIFI_SSID" "$WIFI_PASS" | grep -v '#psk' >> /etc/wpa_supplicant/wpa_supplicant-wlan0.conf
wpa_passphrase "$WIFI_SSID_2G" "$WIFI_PASS_2G" | grep -v '#psk' >> /etc/wpa_supplicant/wpa_supplicant-wlan0.conf
wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant-wlan0.conf
systemctl enable wpa_supplicant@wlan0.service

## Notes
## -------
## udevadm test-builtin net_id /sys/class/net/wlan0 2>&1 | grep ID_NET_NAME_PATH | cut -d= -f2
## -------
## https://wiki.somlabs.com/index.php/Connecting_to_WiFi_network_using_systemd_and_wpa-supplicant
## https://www.networkmanager.dev/docs/api/latest/NetworkManager.conf.html

## Systemd IPv4 Wifi
cat > /etc/systemd/network/wlan0.network <<EOF
[Match]
Name=wlan0
[Network]
DHCP=ipv4
EOF

## Systemd IPv4 Ethernet
cat > /etc/systemd/network/eth0.ipv4.network <<EOF
[Match]
Name=eth0
#MACAddress=

[Network]
DHCP=ipv4
DNS=94.140.14.14                # AdGuard Public DNS
DNS=94.140.15.15                # AdGuard Public DNS
#DNS=1.1.1.1                     # Cloudflare Public DNS
#DNS=1.0.0.1                     # Cloudflare Public DNS
#DNS=8.8.8.8                     # Google Public DNS
#DNS=8.8.4.4                     # Google Public DNS
#DNS=9.9.9.9                     # Quad9 Public DNS
#DNS=149.112.112.112             # Quad9 Public DNS

[Address]
#Address=0.0.0.0/24

[Route]
#Gateway=
EOF

## Systemd IPv6 Ethernet
cat > /etc/systemd/network/eth0.ipv6.network <<EOF
[Match]
Name=eth0
#MACAddress=

[Network]
DHCP=ipv6
IPv6SendRA=yes
DHCPv6PrefixDelegation=yes
DNS=[2a10:50c0::ad1:ff]         # AdGuard Public DNS
DNS=[2a10:50c0::ad2:ff]         # AdGuard Public DNS
#DNS=[2606:4700:4700::1111]      # Cloudflare Public DNS
#DNS=[2606:4700:4700::1001]      # Cloudflare Public DNS
#DNS=[2001:4860:4860::8888]      # Google Public DNS
#DNS=[2001:4860:4860::8844]      # Google Public DNS
#DNS=[2620:fe::fe]               # Quad9 Public DNS
#DNS=[2620:fe::9]                # Quad9 Public DNS

[Address]
#Address=[::]/8

[Route]
#IPv6Preference=high
#Gateway=

[DHCPv6]
PrefixDelegationHint=::/56
EOF

## Restart Networking Services
systemctl restart systemd-networkd
systemctl restart systemd-resolved

## NetworkManager Config
cat > /etc/NetworkManager/NetworkManager.conf <<EOF
[ifupdown]
managed=true

[device]
wifi.backend=iwd
EOF

```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Secure Wipe**

*Commands To Delete Data from Disk and no recovery*

```sh
for x in {1..3}; do
	dd if=/dev/urandom of=/dev/sdX bs=64M status=progress;
	dd if=/dev/zero of=/dev/sdX bs=1M status=progress;
done;
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **OS Partition Backup**

*Commands To Backup OS Partition Table in case of accidental overwrite*

```sh
sfdisk -d /dev/sdX > ~/sdX.dump
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Disk Partition**

*Commands To Partition Disk*

```sh
## Check if system is UEFI Ready
ls /sys/firmware/efi/efivars

## Change Disk to GPT Partition Tables
fdisk /dev/sdX
g
w

## Partition GPT Tables
cgdisk /dev/sdX
## ---
1 - 128M - ef02 - BIOS
2 - 512M - ef00 - UEFI
3 -   8G - 8200 - SWAP
4 - XXXX - 8300 - LINUX
5 - XXXX - 0700 - EXFAT
## ---
0700 - windows fs
8200 - linux swap
8300 - linux fs
8302 - linux /home
8304 - linux /
8310 - linux /var
8311 - linux /var/tmp
8314 - linux /usr
8e00 - linux lvm
ef00 - efi
ef01 - mbr
ef02 - bios
fd00 - linux raid

## Notes
## - Skip UEFI partition if not uefi ready
## - Skip SWAP partition if using swapfile
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Format & Mount Drives**

*Commands To Format Partitions*

```sh
## Notes
## - Skip anything that wasn't partitioned
## - All sdX(#) references are based on previous chart
##   and can change depending on partitioned drive

## Format UEFI Partition
mkfs.fat -F32 /dev/sdX2

## Format SWAP Partition
mkswap /dev/sdX3

## Format LINUX Partition
mkfs.ext4 /dev/sdX4
mkfs.btrfs /dev/sdX4

## Format EXFAT Partion
mkfs.exfat /dev/sdX5
mkfs.ntfs -f /dev/sdX5

## Mount Drives (order important)
mount --mkdir /dev/sdY1 /usb
mount /dev/sdX4 /mnt
mount --mkdir /dev/sdX2 /mnt/boot
swapon /dev/sdX3

## ReMount with Read-Write
mount -o remount,rw /path/to/dir
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Swap File**

*Instructions to create a swap file instead of a swap partition*

```sh
## Opt 1: ext4
touch /mnt/swapfile
chmod 600 /mnt/swapfile
dd if=/dev/zero of=/mnt/swapfile bs=1M count=8192 status=progress
mkswap -U clear /mnt/swapfile
swapon /mnt/swapfile

## Opt 2: btrfs
btrfs subvolume create /mnt/swap
chmod 700 /mnt/swap
touch /mnt/swap/swapfile
chmod 600 /mnt/swap/swapfile
chattr +C /mnt/swap/swapfile
dd if=/dev/zero of=/mnt/swap/swapfile bs=1M count=8192 status=progress
mkswap -U clear /mnt/swap/swapfile
swapon /mnt/swap/swapfile
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Package Repo Setup**

*Commands to generate Arch Linux Package Configuration*

```sh
pacman -Syy

## OPT 1: Get latest 20 repositories
reflector -c US -p https --ipv4 -a 24 -l 100 -n 20 --sort rate --save /etc/pacman.d/mirrorlist

## OPT 2: Get an archived repository
cat > /etc/pacman.d/mirrorlist <<EOF
## reflector -c US -p https --ipv4 -a 24 -l 100 -n 20 --sort rate --save /etc/pacman.d/mirrorlist
Server=https://archive.archlinux.org/repos/2022/07/01/\$repo/os/\$arch
EOF

pacman --noconfirm -S archlinux-keyring

```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **OS Package Install**

*Instructions to add Linux Packages to new drive*

```sh
pacman --noconfirm -S archlinux-keyring

pacstrap /mnt [...packages]

## Core Linux Packages
apparmor archlinux-keyring base base-devel bat bc bluez bridge-utils brotli ca-certificates cdrtools cmake dkms dhcpcd edk2-shell
efibootmgr fastfetch git grub igsc inetutils iw iwd jq libarchive linux linux-firmware linux-headers lxc lxd nano ntp openssl openssh
os-prober opensc openvpn reflector rng-tools sassc sudo systemd virt-firmware wget wireless-regdb udftools unzip usbutils zip

## Programming
sassc prettier uglify-js

## Filesystem Packages
dosfstools e2fsprogs exfatprogs exfat-utils fuse2 fuse3 fuseiso gvfs gvfs-google gvfs-mtp gvfs-smb mtpfs ntfs-3g

## Server Packages
ffmpeg imagemagick libjpeg-turbo libpng librsvg libtiff libwebp nginx nodejs npm p7zip vsftpd

## Database Packages
redis mariadb lmdb leveldb

## Printer Packages
cups cups-pdf cups-filters gutenprint

## Desktop Packages
bluez-utils file-roller gdm gendesk gimp gnome-bluetooth gnome-calculator gnome-control-center gnome-disk-utility gnome-font-viewer
gnome-online-accounts gnome-session gnome-shell gnome-terminal gnome-tweaks gthumb libreoffice-fresh libreoffice-fresh-ru nautilus
networkmanager pulseaudio pulseaudio-bluetooth thunderbird xdg-user-dirs

## Desktop Apps
chromium dconf-editor deepin-screen-recorder gedit firefox filezilla fontforge freerdp libvncserver opera remmina vlc

## Document OCR
tesseract tesseract-data-eng tesseract-data-rus

## VMware Tools
open-vm-tools gtkmm3

```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Fstab Example**

*Instructions to add Linux Packages to new drive*

```sh
## Static information about the filesystems.
## See fstab(5) for details.
## <file system> <dir> <type> <options> <dump> <pass>

## BTRFS SWAP FILE
UUID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX    /swap    btrfs   defaults,subvol=swap    0    0
/swap/swapfile                               none     swap    defaults    0    0

## SWAP FILE
/swapfile                                    none     swap    defaults    0    0

## /dev/sdb3
UUID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX    none     swap    defaults    0    0

## /dev/sdb4
UUID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX    /        ext4    rw,relatime    0    1

## /dev/sdb2
UUID=XXXX-XXXX                               /boot    vfat    rw,relatime,fmask=0022,dmask=0022,codepage=437,iocharset=ascii,shortname=mixed,utf8,errors=remount-ro    0    2
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **System Configuration**

*Update system files for Arch Linux to function properly*

```sh
## Gen / Copy Files
genfstab -U /mnt >> /mnt/etc/fstab
cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist

## Check /mnt/etc/fstab and modify as needed

## Root into new OS
arch-chroot /mnt

## Fix permissions
mkdir -p /usr/local/var/log /usr/local/var/run
chmod 777 /usr/local/var/log /usr/local/var/run
ln -srf /usr/local/var/log /var/local/log
ln -srf /usr/local/var/run /var/local/run

## Download Terminus Font
wget 'https://downloads.sourceforge.net/project/terminus-font/terminus-font-4.49/terminus-font-4.49.1.tar.gz' -O terminus-font-4.49.1.tar.gz
tar -xf terminus-font-4.49.1.tar.gz
cd terminus-font-4.49.1
sudo pacman -S --needed --noconfirm xorg-bdftopcf
./configure
make -j8
sudo make install
cd ../
rm -rf ./terminus-font-4.49.1 ./terminus-font-4.49.1.tar.gz

## Terminal Fixes
setfont /usr/share/kbd/consolefonts/Tamsyn8x16r.psf.gz
tee -a /etc/vconsole.conf > /dev/null <<EOF
FONT=Tamsyn8x16r
EOF
tee -a /etc/inputrc > /dev/null <<EOF

## Turn Off Bell
set bell-style none
EOF

## Configure Date/Time
ln -sf /usr/share/zoneinfo/US/Pacific /etc/localtime
ntpd -qg
hwclock --systohc

## If NTP doesn't set correct time
timedatectl set-timezone America/Los_Angeles
hwclock --set --date="mm/dd/yyyy hh:mm:ss"
hwclock --systohc

## Configure Language
echo 'LANG=en_US.UTF-8' > /etc/locale.conf
sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen
sed -i 's/#ru_RU.UTF-8/ru_RU.UTF-8/g' /etc/locale.gen
locale-gen

## Configure Host
HOSTNAME='arch'
echo "${HOSTNAME}" > /etc/hostname
tee -a /etc/hosts > /dev/null <<EOF
127.0.0.1      localhost
::1            localhost
127.0.1.1      ${HOSTNAME}.localdomain ${HOSTNAME}
EOF

## Configure System Files
tee -a /etc/environment > /dev/null <<EOF
VISUAL=nano
EDITOR=nano
EOF
tee -a /etc/sysctl.d/99-sysctl.conf > /dev/null <<EOF
vm.nr_hugepages = 2048
vm.max_map_count = 262144

## Do less swapping
vm.swappiness = 0
vm.dirty_ratio = 80
vm.dirty_background_ratio = 20
vm.dirty_expire_centisecs = 3000

## Decrease the time for connections to keep alive
net.ipv4.tcp_keepalive_time = 300
net.ipv4.tcp_keepalive_probes = 5
net.ipv4.tcp_keepalive_intvl = 15

## Increase the read-buffer space allocatable
net.ipv4.tcp_rmem = 8192 521728 16777216
net.ipv4.udp_rmem_min = 16384

## Increase the write-buffer-space allocatable
net.ipv4.tcp_wmem = 8192 521728 16777216
net.ipv4.udp_wmem_min = 16384

## Disable IPv6
net.ipv6.conf.all.disable_ipv6=1
net.ipv6.conf.default.disable_ipv6=1

## Allow More File Pointers
fs.inotify.max_user_watches = 524288

## Fix Timeout Errors
kernel.hung_task_timeout_secs = 500
net.ipv4.route.gc_timeout = 1000
net.ipv6.route.gc_timeout = 500
EOF
sysctl --system

## Configure LXC/LXD
touch /etc/sub{u,g}id
usermod --add-subuids 100000-165535 --add-subgids 100000-165535 root

## Add Nano Color Data
mkdir -p /usr/share/nano-extra
wget -q -O - https://github.com/scopatz/nanorc/releases/download/2020.10.10/nanorc-2020.10.10.tar.gz | tar -xz -C /usr/share/nano-extra --wildcards --strip-components 1 */*.nanorc
sed -i ':a;N;$!ba;s|# include "/usr/share/nano/\*.nanorc"|include "/usr/share/nano/*.nanorc"\ninclude "/usr/share/nano-extra/*.nanorc"|g' /etc/nanorc
sed -i ':a;N;$!ba;s|include "/usr/share/nano/\*.nanorc"\n\n|include "/usr/share/nano/*.nanorc"\ninclude "/usr/share/nano-extra/*.nanorc"\n\n|g' /etc/nanorc
sed -i ':a;N;$!ba;s|\nicolor brightnormal|\n#icolor brightnormal|g' /usr/share/nano-extra/nanorc.nanorc
sed -i ':a;N;$!ba;s/color cyan \"(\^'\|'\[\[:space:\]\])#\.\*\$\"/icolor brightblue \"\^\[\[:space:\]\]\*#\.\*\$\"\nicolor cyan \"\^\[\[:space:\]\]\*##\.\*\$\"/g' /usr/share/nano-extra/sh.nanorc
sed -i ':a;N;$!ba;s/color cyan \"(\^'\|'\[\[:blank:\]\])#\.\*\"/icolor brightblue \"\^\[\[:space:\]\]\*#\.\*\$\"\nicolor cyan \"\^\[\[:space:\]\]\*##\.\*\$\"/g' /usr/share/nano/sh.nanorc
sed -i ':a;N;$!ba;s/color cyan \"\^\[\[:blank:\]\]\*#\.\*\"/icolor brightblue \"\^\[\[:space:\]\]\*#\.\*\$\"\nicolor cyan \"\^\[\[:space:\]\]\*##\.\*\$\"/g' /usr/share/nano/default.nanorc

mv /usr/share/nano/default.nanorc /usr/share/nano/default.nanorc.bck

cat > /usr/share/nano/default.nanorc <<EOF
## This is an example of a default syntax.  The default syntax is used for
## files that do not match any other syntax.

syntax default
comment "#"

## Function declarations.
color brightgreen "^[A-Za-z0-9_-]+\\(\\)"

## Keywords, symbols, and comparisons.
color green "\\<(break|case|continue|do|done|elif|else|esac|exit|fi|for|function|if|in|read|return|select|shift|then|time|until|while)\\>"
color green "\\<(declare|eval|exec|export|let|local)\\>"
color green "[][{}():;|\`\$<>!=&\\]"
color green "-(eq|ne|gt|lt|ge|le|ef|ot|nt)\\>"

## Short and long options.
color brightmagenta "[[:blank:]](-[A-Za-z]|--\\<[A-Za-z-]+)\\>"

## Common commands.
color brightblue "\\<(awk|cat|cd|ch(grp|mod|own)|cp|cut|echo|env|grep|head|install|ln|make|mkdir|mv|popd|printf|pushd|rm|rmdir|sed|set|sort|tail|tar|touch|umask|unset)\\>"
color normal "[.-]tar\\>"

## Basic variable names (no braces).
color brightred "\\\$([-@*#?\$!0-9]|[[:alpha:]_][[:alnum:]_]*)"
## More complicated variable names; handles braces and replacements and arrays.
color brightred "\\\$\\{[#!]?([-@*#?\$!]|[0-9]+|[[:alpha:]_][[:alnum:]_]*)(\\[([[:blank:]]*[[:alnum:]_]+[[:blank:]]*|@)\\])?(([#%/]|:?[-=?+])[^}]*\\}|\\[|\\})"

## Comments.
icolor brightblue "^[[:space:]]*#.*\$"
icolor cyan "^[[:space:]]*##.*\$"

## Strings.
color brightyellow ""([^"\\]|\\\\.)*"|'([^'\\]|\\\\.)*'"

## Trailing whitespace.
color ,green "[[:space:]]+\$"
EOF

## Setup Root
## Default root@dmin{mm}{yy}!
groupadd sudo ssh-adm
usermod -a -G ssh-adm root
passwd

## GOTO Install Grub / Grub Update

## Finish Install
exit
swapoff /mnt/swap/swapfile 2>/dev/null
swapoff /mnt/swapfile 2>/dev/null
swapoff /dev/sdX3
umount -R /mnt /usb
reboot
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Grub Install**

*Instructions to install a fresh Grub Instance*

```sh
## Look for other OS on disk
sed -i 's/#GRUB_DISABLE_OS_PROBER/GRUB_DISABLE_OS_PROBER/g' /etc/default/grub

## Check if UEFI
ls -l /sys/firmware/efi/efivars 2>/dev/null | wc -l

## Grub Install [ UEFI ]
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB

## Grub Install [ OTHER ]
grub-install --target=i386-pc /dev/sdX

## Create Grub Config
grub-mkconfig -o /boot/grub/grub.cfg
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Grub Update**

*Instructions to update Grub to include new OS*

```sh
## Opt 1: cmd
update-grub

## Opt 2: Update Grub Config
grub-mkconfig -o /boot/grub/grub.cfg
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **System Configuration Continued**

*Update system files for Arch Linux to function properly*

```sh

## Configure Terminal
tee -a /etc/inputrc > /dev/null <<EOF
set bell-style none
EOF

## Fix Systemd Network Timeout
sed -i ':a;N;$!ba;s|ExecStart=/usr/lib/systemd/systemd-networkd-wait-online\n|ExecStart=/usr/lib/systemd/systemd-networkd-wait-online --any\n|g' /usr/lib/systemd/system/systemd-networkd-wait-online.service

## GOTO: Configure Users

## Enable sudo command [ visudo ]
sed -i 's/# %sudo/%sudo/g' /etc/sudoers

## Start and enable services
systemctl daemon-reload
systemctl reset-failed
systemctl start systemd-networkd systemd-resolved NetworkManager sshd rngd gdm
systemctl enable systemd-networkd systemd-resolved NetworkManager sshd rngd gdm
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Configure Users**

*Commands to create, modify, and delete user accounts*

```sh
## Create Admin User
useradd -m -g users -G wheel,storage,power,lxd,sudo <username>
passwd <username>

## Create Basic User
useradd -m -g users <username>
passwd <username>

## Set passwordless login
passwd -f -d -u <username>

## Add User to SUBUID/SUBGID
usermod --add-subuids ??00000-??65535 --add-subgids ??00000-??65535 <username>
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Generate Moduli**

*Description*

```sh
## Generate Pool ( Pick One )
ssh-keygen -M generate -O bits=4096 moduli.all
ssh-keygen -M generate -O bits=6147 moduli.all
ssh-keygen -M generate -O bits=8196 moduli.all

## Filter Pool and use
ssh-keygen -M screen -O -f moduli.all moduli.safe
mv moduli.safe /etc/ssh/moduli
rm moduli.all
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Resolve Keyring Issues**

*Description*

```sh
## Opt 1: Reinstall keyring
rm /var/lib/pacman/db.lck
pacman -U /var/cache/pacman/pkg/gnupg-*.tar.zst --overwrite "/usr/*"
pacman-key --init
pacman-key --populate archlinux
pacman-key --refresh-keys
pacman -Sy archlinux-keyring

## Opt 2: Disable Signals
tee -a /etc/pacman.conf <<EOF
SigLevel = Never
EOF

## Opt 3: Reset
tee /etc/pacman.d/gnupg/gpg.conf <<EOF
no-greeting
no-permission-warning
lock-never
keyserver-options timeout=10
keyserver-options import-clean
keyserver-options no-self-sigs-only
keyserver hkp://keyserver.ubuntu.com
EOF
pacman -Sc
pacman-key --refresh-keys
pacman -Sy archlinux-keyring
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Gnome Extensions**

*List of useful gnome extensions*

```sh

```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **AUR Package Install**

*List of useful aur packages*

```sh

```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Debtap**

*Description*

```sh
## Convert file from deb to arch pkg
wget https://raw.githubusercontent.com/helixarch/debtap/master/debtap -o $HOME/Downloads/debtap.sh
sudo chown root:root $HOME/Downloads/debtap.sh
sudo chmod 755 $HOME/Downloads/debtap.sh
sudo mv $HOME/Downloads/debtap.sh /usr/bin/debtap

sudo pacman -S pkgfile
sudo debtap -u
debtap <some-package>.deb


```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **WiFi 7 Drivers**

*Wifi 7 ath12k firmware driver install*

```sh
## Wifi Driver Firmware
## https://wireless.wiki.kernel.org/en/users/drivers/ath12k

tee -a /etc/iwd/main.conf > /dev/null <<EOF
[General]
ControlPortOverNL80211=false
EOF

## Check for updates here:
## https://git.codelinaro.org/clo/ath-firmware/ath12k-firmware/

sudo rm -rf /lib/firmware/ath12k/QCN9274 /lib/firmware/ath12k/WCN7850 2>/dev/null
sudo mkdir -p /lib/firmware/ath12k
wget -q -O - https://git.codelinaro.org/clo/ath-firmware/ath12k-firmware/-/archive/main/ath12k-firmware-main.tar.gz | sudo tar -xz -C /lib/firmware/ath12k --strip-components 2 ath12k-firmware-main/ath12k-testing
sudo ln -srf /lib/firmware/ath12k/WCN7850/hw2.0/WLAN.HMT.1.0.c5-00481-QCAHMTSWPL_V1.0_V2.0_SILICONZ-3/m3.bin /lib/firmware/ath12k/WCN7850/hw2.0/
sudo ln -srf /lib/firmware/ath12k/WCN7850/hw2.0/WLAN.HMT.1.0.c5-00481-QCAHMTSWPL_V1.0_V2.0_SILICONZ-3/amss.bin /lib/firmware/ath12k/WCN7850/hw2.0/
sudo ln -srf /lib/firmware/ath12k/QCN9274/hw2.0/WLAN.HMT.1.0.c5-00481-QCAHMTSWPL_V1.0_V2.0_SILICONZ-3/m3.bin /lib/firmware/ath12k/QCN9274/hw2.0/
sudo ln -srf /lib/firmware/ath12k/QCN9274/hw2.0/WLAN.HMT.1.0.c5-00481-QCAHMTSWPL_V1.0_V2.0_SILICONZ-3/amss.bin /lib/firmware/ath12k/QCN9274/hw2.0/

```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Get System Information**

*Description*

```sh
dmidecode | less
lspci | sort
lsusb | sort
df -h | sort
free -h
ip -br a
lsblk
blkid
dmesg --level emerg,alert,crit,err
lsb_release
cat /etc/os-release
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Title**

*Description*

```sh

```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;
