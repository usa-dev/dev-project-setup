### [RETURN TO README](README.md#markdown-header-table-of-contents)
---

 &nbsp; &nbsp;




### **Table of Contents**
- [File Modifications](#markdown-header-file-modifications) | *Update system files for VSCode to function properly*
- [Update Settings](#markdown-header-update-settings) | *Edit the settings in json format*
- [Keyboard Shortcuts](#markdown-header-keyboard-shortcuts) | *Edit the keyboard shortcuts in json format*
- [Extensions](#markdown-header-extensions) | *Download and install vscode extensions*
- [User Snippets](#markdown-header-user-snippets) | *Code snippets - commonly used code pieces*

 &nbsp; &nbsp;




### **File Modifications**

*Update system files for VSCode to function properly*

```sh
sudo tee -a /etc/sysctl.d/99-sysctl.conf > /dev/null <<EOF
fs.inotify.max_user_watches=524288
EOF

sudo sysctl -p
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Update Settings**

*Edit the settings in json format*

```json
// File > Preferences > Settings > {} (Top Right)
{
    "[dot]": {
        "workbench.editor.languageDetection": false
    },
    "diffEditor.codeLens": true,
    "diffEditor.wordWrap": "off",
    "editor.accessibilitySupport": "off",
    "editor.codeLens": false,
    "editor.defaultFormatter": "esbenp.prettier-vscode",
    "editor.folding": false,
    "editor.foldingMaximumRegions": 1000,
    "editor.fontFamily": "'Fira Code', 'Noto Sans Mono', 'Roboto Mono', 'San Francisco Mono', 'Liberation Mono', 'Monospace'",
    "editor.fontSize": 12,
    "editor.formatOnPaste": false,
    "editor.formatOnSave": false,
    "editor.insertSpaces": false,
    "editor.minimap.showSlider": "always",
    "editor.stickyScroll.enabled": false,
    "editor.unicodeHighlight.ambiguousCharacters": true,
    "editor.wordWrap": "off",
    "editor.wordWrapColumn": 1000,
    "emmet.excludeLanguages": [
        "markdown",
        "php"
    ],
    "explorer.confirmDelete": false,
    "extensions.ignoreRecommendations": true,
    "files.associations": {
        "*.def": "properties",
        "*.dot": "dotjs",
        "*.env": "ini",
        "*.env.*": "ini",
        "*.gitignore": "properties",
        "*.js": "javascript",
        "*.jsd": "javascript",
        "*.json": "json",
        "*.vz": "dot"
    },
    "files.autoSave": "afterDelay",
    "files.autoSaveDelay": 1000,
    "files.trimFinalNewlines": true,
    "files.trimTrailingWhitespace": true,
    "files.watcherExclude": {
        "**/.git/**": true,
        "**/node_modules/**": true
    },
    "git.openRepositoryInParentFolders": "never",
    "gremlins.showInProblemPane": true,
    "html.format.wrapLineLength": 1000,
    "javascript.suggest.autoImports": false,
    "javascript.updateImportsOnFileMove.enabled": "never",
    "json.maxItemsComputed": 100000,
    "liveServer.settings.donotShowInfoMsg": true,
    "liveServer.settings.donotVerifyTags": true,
    "liveServer.settings.host": "0.0.0.0",
    "liveServer.settings.https": {
        "cert": "/home/$USER/Projects/liveserver.crt",
        "enable": true,
        "key": "/home/$USER/Projects/liveserver.key",
        "passphrase": ""
    },
    "liveServer.settings.wait": 3000,
    "php.suggest.basic": false,
    "php.validate.enable": false,
    "prettier.arrowParens": "always",
    "prettier.bracketSameLine": true,
    "prettier.bracketSpacing": true,
    "prettier.embeddedLanguageFormatting": "auto",
    "prettier.endOfLine": "lf",
    "prettier.htmlWhitespaceSensitivity": "css",
    "prettier.insertPragma": false,
    "prettier.printWidth": 300,
    "prettier.proseWrap": "preserve",
    "prettier.quoteProps": "as-needed",
    "prettier.requirePragma": false,
    "prettier.semi": true,
    "prettier.singleAttributePerLine": false,
    "prettier.singleQuote": false,
    "prettier.tabWidth": 4,
    "prettier.trailingComma": "es5",
    "prettier.useTabs": true,
    "projectManager.any.baseFolders": [
        "/home/$USER/Projects",
        "/mnt/home/$USER/Projects"
    ],
    "projectManager.any.maxDepthRecursion": 1,
    "scm.alwaysShowActions": true,
    "scm.alwaysShowRepositories": true,
    "security.workspace.trust.untrustedFiles": "open",
    "telemetry.telemetryLevel": "off",
    "terminal.integrated.fontSize": 12,
    "window.menuBarVisibility": "classic",
    "window.restoreWindows": "none",
    "workbench.editor.enablePreview": false,
    "workbench.iconTheme": "vs-seti",
    "workbench.sideBar.location": "left",
    "workbench.startupEditor": "none",
    "workbench.statusBar.visible": true
}
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Keyboard Shortcuts**

*Edit the keyboard shortcuts in json format*

```json
// File > Preferences > Keyboard Shortcuts > {} (Top Right)
[{
    "key": "ctrl+shift+w",
    "command": "workbench.action.files.openFolder"
},{
    "key": "ctrl+a",
    "command": "editor.action.selectAll"
},{
    "key": "ctrl+alt+a",
    "command": "editor.action.commentLine",
    "when": "editorTextFocus && !editorReadonly"
},{
    "key": "ctrl+shift+a",
    "command": "editor.action.blockComment",
    "when": "editorTextFocus && !editorReadonly"
},{
    "key": "ctrl+d",
    "command": "editor.action.copyLinesDownAction",
    "when": "editorTextFocus && !editorReadonly"
},{
    "key": "ctrl+shift+d",
    "command": "editor.action.duplicateSelection",
    "when": "editorTextFocus && !editorReadonly"
},{
    "key": "ctrl+k",
    "command": "editor.action.deleteLines",
    "when": "textInputFocus && !editorReadonly"
}]


// Depricated
[{
	"key": "ctrl+shift+d",
	"command": "editor.action.copyLinesUpAction"
},{
	"key": "ctrl+shift+k",
	"command": "editor.action.deleteLines",
	"when": "textInputFocus && !editorReadonly"
},{
	"key": "ctrl+1",
	"command": "extension.multiCommand.execute",
	"args": {
		"sequence": ["extension.uglifyJson", "HookyQR.beautifyFile"]
	}
},{
	"key": "ctrl+2",
	"command": "extension.uglifyJson"
},{
	"key": "ctrl+3",
	"command": "HookyQR.beautifyFile"
}]
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Extensions**

*Download and install vscode extensions*
```sh
# Linux Install
pacman -S sassc prettier uglify-js

# NPM Global Apps
npm i -g json5 @prettier/plugin-php @prettier/plugin-xml prettier-plugin-sh

# View > Extensions
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
allentong.dotjs							dotjs-syntax							https://marketplace.visualstudio.com/items?itemName=allentong.dotjs
joaompinto.vscode-graphviz				Graphviz \(dot\) language				https://marketplace.visualstudio.com/items?itemName=joaompinto.vscode-graphviz
nhoizey.gremlins						Gremlins tracker						https://marketplace.visualstudio.com/items?itemName=nhoizey.gremlins
p42ai.refactor							JavaScript Refactoring Assistant		https://marketplace.visualstudio.com/items?itemName=p42ai.refactor
mrmlnc.vscode-json5						JSON5 syntax							https://marketplace.visualstudio.com/items?itemName=mrmlnc.vscode-json5
KatjanaKosic.vscode-json5				JSON5 syntax extended					https://marketplace.visualstudio.com/items?itemName=KatjanaKosic.vscode-json5
ritwickdey.liveserver					Live Server								https://marketplace.visualstudio.com/items?itemName=ritwickdey.liveserver
ryuta46.multi-command					multi-command							https://marketplace.visualstudio.com/items?itemName=ryuta46.multi-command
almir.nginx-syntax-highlighter			Nginx Syntax Highlighter				https://marketplace.visualstudio.com/items?itemName=almir.nginx-syntax-highlighter
hangxingliu.vscode-nginx-conf-hint		nginx.conf hint							https://marketplace.visualstudio.com/items?itemName=hangxingliu.vscode-nginx-conf-hint
DEVSENSE.phptools-vscode				PHP Tools								https://marketplace.visualstudio.com/items?itemName=DEVSENSE.phptools-vscode
esbenp.prettier-vscode					Prettier - Code formatter				https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode
richie5um2.vscode-sort-json				Sort JSON objects						https://marketplace.visualstudio.com/items?itemName=richie5um2.vscode-sort-json
cstrachan.vcard							vCard									https://marketplace.visualstudio.com/items?itemName=cstrachan.vcard
alefragnani.project-manager             Project Manager                         https://open-vsx.org/extension/alefragnani/project-manager
# --------------------------------------------------------------------------------------------------------------------------------------------------------------


# View > Extensions (Depricated)
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
HookyQR.beautify						beautify								https://marketplace.visualstudio.com/items?itemName=HookyQR.beautify
teddylun.json-utils						JSON Utils								https://marketplace.visualstudio.com/items?itemName=teddylun.json-utils
ritwickdey.live-sass					Live Sass Compiler						https://marketplace.visualstudio.com/items?itemName=ritwickdey.live-sass
HookyQR.minify							Minify									https://marketplace.visualstudio.com/items?itemName=HookyQR.minify
ahmadalli.vscode-nginx-conf				NGINX Configuration Language Support	https://marketplace.visualstudio.com/items?itemName=ahmadalli.vscode-nginx-conf
raynigon.nginx-formatter				nginx-formatter							https://marketplace.visualstudio.com/items?itemName=raynigon.nginx-formatter
andyyaldoo.vscode-json					vscode-json								https://marketplace.visualstudio.com/items?itemName=andyyaldoo.vscode-json
Vue.volar								Vue Language Features \(Volar\)			https://marketplace.visualstudio.com/items?itemName=Vue.volar
# --------------------------------------------------------------------------------------------------------------------------------------------------------------
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Liveserver HTTPS Settings**

*Code snippets - commonly used code pieces*

```sh
## cd Projects Folder

openssl req -newkey rsa:4096 -x509 -sha512 -days 3650 -nodes -out liveserver.crt -keyout liveserver.key -subj "/C=US/ST=CA/L=NA/O=SEC/OU=IT/CN=localhost"
openssl pkcs12 -export -inkey liveserver.key -in liveserver.crt -out liveserver.p12

## Update vscode settings full path
## Add p12 to browser certificates

```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **User Snippets**

*Code snippets - commonly used code pieces*

```json
// File > Preferences > User Snippets > New Global Snippets File > global
// https://code.visualstudio.com/docs/editor/userdefinedsnippets#_snippet-syntax
{
	"JSDoc MultiLine Comment Header":{
		"scope": "javascript,typescript",
		"description": "JSDoc MultiLine Comment Header",
		"body": [
			"/**",
			" * @name CodeDocProcedure | Code Documentation & Comment Procedure",
			" * @description See links to learn how to add documentation comments and version control to your code.",
			" * @see {@link https://jsdoc.app/ | JSDOC}",
			" * @see {@link https://semver.org/ | SEMVER}",
			" */",
			"",
			"",
			"",
			"$0"],
		"prefix": "comment-header-jsdoc"
	},
	"JSDoc Function Comment":{
		"scope": "javascript,typescript",
		"description": "JSDoc Function Comment",
		"body": [
			"/**",
			" * @name $1",
			" * @description $2",
			" */",
			"$0"],
		"prefix": "comment-function-jsdoc"
	},
	"JSDoc Function Comment With Version":{
		"scope": "javascript,typescript",
		"description": "JSDoc Function Comment With Version",
		"body": [
			"/**",
			" * @name $1",
			" * @description $2",
			" * @version $3",
			" */",
			"$0"],
		"prefix": "comment-function-version-jsdoc"
	}
}
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;