### [RETURN TO README](README.md#markdown-header-table-of-contents)
---

 &nbsp; &nbsp;




### **Table of Contents**
- [Shell Notes](#markdown-header-shell-notes) | *Shell Notes*
- [Shell RC](#markdown-header-shell-rc) | *Terminal Settings*
- [Shell Profile](#markdown-header-shell-profile) | *Login Terminal Settings*
- [Shell Aliases](#markdown-header-shell-aliases) | *Terminal Aliases Settings*
- [Root Bash History](#markdown-header-root-bash-history) | *Bash commands that are used by root*
- [User Bash History](#markdown-header-user-bash-history) | *Bash commands that are used by users*

 &nbsp; &nbsp;




### **Shell Notes**

*Shell Notes*

```sh
## Installed Shells
cat /etc/shells

## To disable login messages
cat > ~/.hushlogin <<EOF
no-login-message
EOF

## Fix terminal colors on none GUI instances
tee -a ~/.shrc <<EOF
## No GUI fix colors
TERM=xterm-256color
EOF

## Check Colors ( Simple )
for c in {0..255}; do tput setaf $c; printf "%03d " $c; done

## Check Colors ( Columns )
for c in {0..255}; do tput setaf $c; printf "%03d " $c; if test $c -ne 0 -a $(( $c % 16 )) -eq 0; then printf "\n"; fi; done
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Shell RC**

*Terminal Settings*

```sh
cat > ~/.shrc <<EOF
## --------------------------------
## Method Declaration
## --------------------------------

append_path () {
    case ":\$PATH:" in
        *:"\$1":*) ;;
        *) PATH="\${PATH:+\$PATH:}\${1}" ;;
    esac
}

prepend_path () {
    case ":\$PATH:" in
        *:"\$1":*) ;;
        *) PATH="\${1}\${PATH:+:\$PATH}" ;;
    esac
}

create_symlink () {
    if test ! -e \$1; then return 1; fi
    if test -e \$2; then return 1; fi
    ln -frs \$1 \$2
}



## --------------------------------
## Main Script
## --------------------------------



## Get environment variables
set -a
source /etc/environment
set +a



## Link to shell rc
if test -f \$HOME/.shrc; then
    if test -f /bin/bash; then
        create_symlink "\${HOME}/.shrc" "\${HOME}/.bashrc"
    fi
    if test -f /bin/dash; then
        create_symlink "\${HOME}/.shrc" "\${HOME}/.dashrc"
    fi
    if test -f /bin/ash; then
        create_symlink "\${HOME}/.shrc" "\${HOME}/.ashrc"
    fi
    if test -f /bin/csh; then
        create_symlink "\${HOME}/.shrc" "\${HOME}/.cshrc"
    fi
    if test -f /bin/tcsh; then
        create_symlink "\${HOME}/.shrc" "\${HOME}/.tcshrc"
    fi
    if test -f /bin/ksh; then
        create_symlink "\${HOME}/.shrc" "\${HOME}/.kshrc"
    fi
    if test -f /bin/zsh; then
        create_symlink "\${HOME}/.shrc" "\${HOME}/.zshrc"
    fi
fi



## Link to shell aliases
if test -f \$HOME/.aliases; then
    if test -f /bin/bash; then
        create_symlink "\${HOME}/.aliases" "\${HOME}/.bash_aliases"
    fi
    if test -f /bin/dash; then
        create_symlink "\${HOME}/.aliases" "\${HOME}/.dash_aliases"
    fi
    if test -f /bin/ash; then
        create_symlink "\${HOME}/.aliases" "\${HOME}/.ash_aliases"
    fi
    if test -f /bin/csh; then
        create_symlink "\${HOME}/.aliases" "\${HOME}/.csh_aliases"
    fi
    if test -f /bin/tcsh; then
        create_symlink "\${HOME}/.aliases" "\${HOME}/.tcsh_aliases"
    fi
    if test -f /bin/ksh; then
        create_symlink "\${HOME}/.aliases" "\${HOME}/.ksh_aliases"
    fi
    if test -f /bin/zsh; then
        create_symlink "\${HOME}/.aliases" "\${HOME}/.zsh_aliases"
    fi
fi



## Link to shell history
if test -f \$HOME/.history; then
    if test -f /bin/bash; then
        create_symlink "\${HOME}/.history" "\${HOME}/.bash_history"
    fi
    if test -f /bin/dash; then
        create_symlink "\${HOME}/.history" "\${HOME}/.dash_history"
    fi
    if test -f /bin/ash; then
        create_symlink "\${HOME}/.history" "\${HOME}/.ash_history"
    fi
    if test -f /bin/csh; then
        create_symlink "\${HOME}/.history" "\${HOME}/.csh_history"
    fi
    if test -f /bin/tcsh; then
        create_symlink "\${HOME}/.history" "\${HOME}/.tcsh_history"
    fi
    if test -f /bin/ksh; then
        create_symlink "\${HOME}/.history" "\${HOME}/.ksh_history"
    fi
    if test -f /bin/zsh; then
        create_symlink "\${HOME}/.history" "\${HOME}/.zsh_history"
    fi
fi



## Get aliases
case "\${SHELL}" in
    /bin/bash) source \$HOME/.bash_aliases 2>/dev/null ;;
    /bin/dash) source \$HOME/.dash_aliases 2>/dev/null ;;
    /bin/ash) source \$HOME/.ash_aliases 2>/dev/null ;;
    /bin/csh) source \$HOME/.csh_aliases 2>/dev/null ;;
    /bin/tcsh) source \$HOME/.tcsh_aliases 2>/dev/null ;;
    /bin/ksh) source \$HOME/.ksh_aliases 2>/dev/null ;;
    /bin/zsh) source \$HOME/.zsh_aliases 2>/dev/null ;;
    *) source \$HOME/.aliases 2>/dev/null ;;
esac



## Define path variable
PATH="\${PATH:-''}"
prepend_path "/sbin"
prepend_path "/usr/sbin"
prepend_path "/usr/local/sbin"
prepend_path "/bin"
prepend_path "/usr/bin"
prepend_path "/usr/local/bin"

if test "\${USER}" != "root"; then
    prepend_path "\${HOME}/bin"
    prepend_path "\${HOME}/.local/bin"
fi



## Terminal Color Definitions
##
## [CLR]             = Clear Styles
## [R|B]             = Regular, Bold
## [D|L]             = Dark, Light
## [N|R|G|Y|B|M|C|W] = Black, Red, Green, Yellow, Blue, Magenta, Cyan, White
##
CLR='\\[\\033[0m\\]'

RDN='\\[\\033[0;38;5;0m\\]'
RDR='\\[\\033[0;38;5;1m\\]'
RDG='\\[\\033[0;38;5;2m\\]'
RDY='\\[\\033[0;38;5;3m\\]'
RDB='\\[\\033[0;38;5;4m\\]'
RDM='\\[\\033[0;38;5;5m\\]'
RDC='\\[\\033[0;38;5;6m\\]'
RDW='\\[\\033[0;38;5;7m\\]'

RLN='\\[\\033[0;38;5;8m\\]'
RLR='\\[\\033[0;38;5;9m\\]'
RLG='\\[\\033[0;38;5;10m\\]'
RLY='\\[\\033[0;38;5;11m\\]'
RLB='\\[\\033[0;38;5;12m\\]'
RLM='\\[\\033[0;38;5;13m\\]'
RLC='\\[\\033[0;38;5;14m\\]'
RLW='\\[\\033[0;38;5;15m\\]'

BDN='\\[\\033[1;38;5;0m\\]'
BDR='\\[\\033[1;38;5;1m\\]'
BDG='\\[\\033[1;38;5;2m\\]'
BDY='\\[\\033[1;38;5;3m\\]'
BDB='\\[\\033[1;38;5;4m\\]'
BDM='\\[\\033[1;38;5;5m\\]'
BDC='\\[\\033[1;38;5;6m\\]'
BDW='\\[\\033[1;38;5;7m\\]'

BLN='\\[\\033[1;38;5;8m\\]'
BLR='\\[\\033[1;38;5;9m\\]'
BLG='\\[\\033[1;38;5;10m\\]'
BLY='\\[\\033[1;38;5;11m\\]'
BLB='\\[\\033[1;38;5;12m\\]'
BLM='\\[\\033[1;38;5;13m\\]'
BLC='\\[\\033[1;38;5;14m\\]'
BLW='\\[\\033[1;38;5;15m\\]'



## Terminal Structure Definitions
PRE="\$RLN*\$CLR"
DIV="\$PRE \$BDN--------------------------------------------------------------\$CLR"



## Terminal User Select
##
## Root = Red
## Admin = Blue
## User = Green
##
case "\$(groups)" in
    *root*)             PS1="\\n\$DIV\\n\$PRE \$RDC\\t\$CLR \$RLN|\$CLR \$RLN\\\$PWD\$CLR \\n\$PRE \$BDR\\u@\\H\$CLR# " ;;
    *wheel*|*sudo*)     PS1="\\n\$DIV\\n\$PRE \$RDC\\t\$CLR \$RLN|\$CLR \$RLN\\\$PWD\$CLR \\n\$PRE \$BDB\\u@\\H\$CLR\$ " ;;
    *)                  PS1="\\n\$DIV\\n\$PRE \$RDC\\t\$CLR \$RLN|\$CLR \$RLN\\\$PWD\$CLR \\n\$PRE \$BDG\\u@\\H\$CLR\$ " ;;
esac
EOF
source ~/.shrc
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Shell Profile**

*Login Terminal Settings*

```sh
cat > ~/.profile <<EOF
## Script executed on user login

if test -f "\${HOME}/.shrc"; then
	source \$HOME/.shrc 2>/dev/null
elif test -f "\${HOME}/.bashrc"; then
	source \$HOME/.bashrc 2>/dev/null
elif test -f "\${HOME}/.ashrc"; then
	source \$HOME/.ashrc 2>/dev/null
elif test -f "\${HOME}/.dashrc"; then
	source \$HOME/.dashrc 2>/dev/null
elif test -f "\${HOME}/.cshrc"; then
	source \$HOME/.cshrc 2>/dev/null
elif test -f "\${HOME}/.tcshrc"; then
	source \$HOME/.tcshrc 2>/dev/null
elif test -f "\${HOME}/.kshrc"; then
	source \$HOME/.kshrc 2>/dev/null
elif test -f "\${HOME}/.zshrc"; then
	source \$HOME/.zshrc 2>/dev/null
fi
EOF
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Shell Aliases**

*Terminal Aliases Settings*

```sh
cat > ~/.aliases <<EOF
## --------------------------------
## Server Aliases
## --------------------------------

alias ls='ls --color=auto'
alias ll='ls --color=auto -lh'
alias la='ls --color=auto -lhA'

## Required run aliases
alias sudo='sudo '

alias nano='nano -li -T 4'
alias sunano='sudo nano -li -T 4'

alias bat='bat -pp -f -l sh'
alias subat='sudo bat -pp -f -l sh'

#alias bat='batcat -pp --color always -l sh'
#alias subat='sudo batcat -pp --color always -l sh'

alias vpnbookpass='tesseract "https://www.vpnbook.com/password.php?t=$(date +%s.%N)" - -l eng'

alias mountshares='mkdir -p /tmp/shares && vmhgfs-fuse .host:/ /tmp/shares -o subtype=vmhgfs-fuse,allow_other'
alias sumountshares='sudo mkdir -p /tmp/shares && sudo vmhgfs-fuse .host:/ /tmp/shares -o subtype=vmhgfs-fuse,allow_other'



## --------------------------------
## Desktop Aliases
## --------------------------------

alias chromedev='chromium --disable-web-security --user-data-dir="/home/$USER/.cache/chromium-dev" 2>/dev/null &'
alias sucode='sudo code --new-window --no-sandbox --disable-gpu-sandbox --disable-workspace-trust --user-data-dir="/home/$USER/.cache/code"'

EOF
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Root Bash History**

*Bash commands that are used by root*

```sh
cat > ~/.history <<EOF
history -c && exit
history -c && shutdown -r now
nano ~/.history
pacman -Syyu --noconfirm
pacman -S
pacman -R
openvpn --config /vpn/vpnbook-ca222-tcp80.ovpn --auth-user-pass "\$(vpnbookpass)"
vmware-modconfig --console --install-all
ntpd -gq && hwclock --systohc
git add -A && git commit -m "" && git pull && git push -u origin main
git pull && git push -u origin main





EOF
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **User Bash History**

*Bash commands that are used by users*

```sh
cat > ~/.history <<EOF
history -c && exit
history -c && sudo shutdown -r now
nano ~/.history
sudo pacman -Syyu --noconfirm
sudo pacman -S
sudo pacman -R
sudo openvpn --config /vpn/vpnbook-ca222-tcp80.ovpn --auth-user-pass "$(vpnbookpass)"
sudo vmware-modconfig --console --install-all
sudo ntpd -gq && sudo hwclock --systohc
git add -A && git commit -m "" && git pull && git push -u origin main
git pull && git push -u origin main






EOF
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;
