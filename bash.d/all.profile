## Script executed on user login

if test -f "${HOME}/.shrc"; then
	source $HOME/.shrc 2>/dev/null
elif test -f "${HOME}/.bashrc"; then
	source $HOME/.bashrc 2>/dev/null
elif test -f "${HOME}/.ashrc"; then
	source $HOME/.ashrc 2>/dev/null
elif test -f "${HOME}/.dashrc"; then
	source $HOME/.dashrc 2>/dev/null
elif test -f "${HOME}/.cshrc"; then
	source $HOME/.cshrc 2>/dev/null
elif test -f "${HOME}/.tcshrc"; then
	source $HOME/.tcshrc 2>/dev/null
elif test -f "${HOME}/.kshrc"; then
	source $HOME/.kshrc 2>/dev/null
elif test -f "${HOME}/.zshrc"; then
	source $HOME/.zshrc 2>/dev/null
fi
