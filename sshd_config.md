### [RETURN TO README](README.md#markdown-header-table-of-contents)
---

 &nbsp; &nbsp;




### **Table of Contents**
- [File sshd_config](#markdown-sshd_config) | *Setup sshd_config file*

 &nbsp; &nbsp;




### **sshd_config**
*Setup git global config*
```sh
#-----------------------------------------------------------------------#
# Name: sshd_config                                                     #
# Date: 2022-02-12                                                      #
# Link: https://man.archlinux.org/man/sshd_config.5                     #
# Link: https://www.man7.org/linux/man-pages/man5/sshd_config.5.html    #
#                                                                       #
#-----------------------------------------------------------------------#
# DEFINITIONS                                                           #
#-----------------------------------------------------------------------#
# MCD = Multiple Select Comma Delimited                                 #
# MSD = Multiple Select Space Delimited                                 #
#-----------------------------------------------------------------------#
# PATTERNS                                                              #
#-----------------------------------------------------------------------#
# [0-9A-Za-z] matches alphabet & numbers                                #
# (`*`) matches 0+ char                                                 #
# (`?`) matches 1 char                                                  #
# (`!`) negates match                                                   #
#-----------------------------------------------------------------------#

## Include Overwrites
Include /etc/ssh/sshd_config.d/*.conf

#AcceptEnv LANG LC_*                                             ## [MSD] Specifies what environment variables sent by the client                                                            # PATTERNS
AddressFamily any                                                ## Specifies which address family should be used                                                                            # any | inet | inet6
AllowAgentForwarding no                                          ## Specifies whether ssh-agent forwarding is permitted                                                                      # yes | no
AllowGroups ssh-adm                                              ## [MSD] A list of group name patterns (Order: DenyGroups, AllowGroups)                                                     # PATTERNS
AllowStreamLocalForwarding no                                    ## Specifies whether Unix-domain socket is permitted                                                                        # yes | no | all | local | remote
AllowTcpForwarding no                                            ## Specifies whether TCP forwarding is permitted                                                                            # yes | no | all | local | remote
#AllowUsers                                                      ## [MSD] A list of user name patterns (Order: DenyUsers, AllowUsers)                                                        # PATTERNS
AuthenticationMethods publickey                                  ## [MCD] Specifies the authentication methods                                                                               # publickey | password | none | keyboard-interactive | hostbased | gssapi-with-mic
#AuthorizedKeysCommand                                           ## Specifies a program to be used to look up the user's public keys                                                         # Absolute path to program
#AuthorizedKeysCommandUser                                       ## Specifies the user under whose account the AuthorizedKeysCommand is run                                                  # username
AuthorizedKeysFile /etc/ssh/authorized_keys                      ## [MSD] Specifies the file that contains the public keys used for user auth                                                # none | path to program
#AuthorizedPrincipalsCommand                                     ## Specifies a program to be used to generate the list of allowed certificate principals                                    # Absolute path to program
#AuthorizedPrincipalsCommandUser                                 ## Specifies the user under whose account the AuthorizedPrincipalsCommand is run                                            # username
AuthorizedPrincipalsFile none                                    ## Specifies a file that lists principal names that are accepted for certificate authentication                             # none | path to program
Banner none                                                      ## The contents of the specified file are sent to the remote user before authentication is allowed                          # none | absolute path to file
CASignatureAlgorithms ssh-ed25519,sk-ssh-ed25519@openssh.com     ## [MCD] Specifies which algorithms are allowed for signing of certificates by certificate authorities                      # ssh-ed25519,ecdsa-sha2-nistp256,ecdsa-sha2-nistp384,ecdsa-sha2-nistp521,sk-ssh-ed25519@openssh.com,sk-ecdsa-sha2-nistp256@openssh.com,rsa-sha2-512,rsa-sha2-256
ChrootDirectory none                                             ## Specifies the pathname of a directory to chroot to after authentication                                                  # none | absolute path
Ciphers chacha20-poly1305@openssh.com,aes256-gcm@openssh.com     ## [MCD] Specifies the ciphers allowed                                                                                      # chacha20-poly1305@openssh.com,aes256-gcm@openssh.com,aes128-gcm@openssh.com,aes256-ctr,aes192-ctr,aes128-ctr,aes256-cbc,aes192-cbc,aes128-cbc,3des-cbc
ClientAliveCountMax 2                                            ## Sets the number of client alive messages which may be sent without sshd receiving any messages back                      # NUMBER_INT in seconds
ClientAliveInterval 300                                          ## Sets a timeout interval check in seconds after which no data has been received from the client                           # NUMBER_INT in seconds
Compression yes                                                  ## Specifies whether compression is enabled                                                                                 # yes | no
#DenyGroups                                                      ## [MSD] A list of group name patterns (Order: DenyGroups, AllowGroups)                                                     # PATTERNS
#DenyUsers                                                       ## [MSD] A list of user name patterns (Order: DenyUsers, AllowUsers)                                                        # PATTERNS
DisableForwarding yes                                            ## Disables all forwarding features, including X11, ssh-agent, TCP and StreamLocal                                          # yes | no
ExposeAuthInfo no                                                ## Writes a temporary file containing a list of authentication methods and public credentials                               # yes | no
FingerprintHash sha256                                           ## Specifies the hash algorithm used when logging key fingerprints                                                          # sha256 | md5
#ForceCommand none                                               ## Forces the execution of the command                                                                                      # none | command
GatewayPorts no                                                  ## Specifies whether remote hosts are allowed to connect to ports forwarded for the client                                  # yes | no | clientspecified
#GSSAPIAuthentication no                                         ## Specifies whether user authentication based on GSSAPI is allowed                                                         # yes | no
#GSSAPICleanupCredentials yes                                    ## Specifies whether to automatically destroy the user's credentials cache on logout                                        # yes | no
#GSSAPIStrictAcceptorCheck yes                                   ## Determines whether to be strict about the identity of the GSSAPI acceptor                                                # yes | no
HostbasedAuthentication no                                       ## Specifies whether rhosts or /etc/hosts.equiv authentication is allowed                                                   # yes | no
HostbasedUsesNameFromPacketOnly no                               ## Specifies whether or not the server will attempt to perform a reverse name lookup during HostbasedAuthentication         # yes | no
#HostCertificate                                                 ## [MCD] Specifies a file containing a public host certificate                                                              # /etc/ssh/ssh_host_ecdsa_key.pub,/etc/ssh/ssh_host_ed25519_key.pub,/etc/ssh/ssh_host_dsa_key.pub,/etc/ssh/ssh_host_rsa_key.pub
HostKey /etc/ssh/ssh_host_ed25519_key                            ## [MCD] Specifies a file containing a private host key used by SSH                                                         # /etc/ssh/ssh_host_ecdsa_key,/etc/ssh/ssh_host_ed25519_key,/etc/ssh/ssh_host_dsa_key,/etc/ssh/ssh_host_rsa_key
#HostKeyAgent                                                    ## Identifies the UNIX-domain socket used to communicate with an agent                                                      #
IgnoreRhosts yes                                                 ## Specifies whether to ignore per-user .rhosts and .shosts files during HostbasedAuthentication                            # yes | no | shosts-only
IgnoreUserKnownHosts yes                                         ## Specifies whether sshd should ignore the user's ~/.ssh/known_hosts during HostbasedAuthentication                        # yes | no
#Include                                                         ## [MCD] Include the specified configuration file(s)                                                                        # Absolute path
IPQoS af21 cs1                                                   ## Specifies the IPv4 type-of-service or DSCP class for the connection                                                      # af11 | af12 | af13 | af21 | af22 | af23 | af31 | af32 | af33 | af41 | af42 | af43 | cs0 | cs1 | cs2 | cs3 | cs4 | cs5 | cs6 | cs7 | ef | le | lowdelay | throughput | reliability | NUMBER_INT | none
KbdInteractiveAuthentication no                                  ## Specifies whether to allow keyboard-interactive authentication                                                           # yes | no
#KerberosAuthentication no                                       ## Specifies whether the password provided for PasswordAuthentication will be validated through the Kerberos KDC            # yes | no
#KerberosGetAFSToken no                                          ## If AFS is active and the user has a Kerberos 5 TGT, attempt to acquire an AFS token                                      # yes | no
#KerberosOrLocalPasswd no                                        ## If password authentication through Kerberos fails then the password will be validated                                    # yes | no
#KerberosTicketCleanup yes                                       ## Specifies whether to automatically destroy the user's ticket cache file on logout                                        # yes | no
#ListenAddress                                                   ## Specifies the local addresses sshd should listen on                                                                      # HOST:PORT
LoginGraceTime 30                                                ## The server disconnects after this time if the user has not successfully logged in                                        # NUMBER_INT in seconds
LogLevel VERBOSE                                                 ## Gives the verbosity level that is used when logging messages from sshd                                                   # QUIET | FATAL | ERROR | INFO | VERBOSE | DEBUG | DEBUG1 | DEBUG2 | DEBUG3
#LogVerbose                                                      ## Specify one or more overrides to LogLevel                                                                                #
#Match                                                           ## Introduces a conditional block                                                                                           #
MaxAuthTries 3                                                   ## Specifies the maximum number of authentication attempts permitted per connection                                         # NUMBER_INT
MaxSessions 4                                                    ## Specifies the maximum number of open shell, login or subsystem (e.g. sftp) sessions permitted per network connection     # NUMBER_INT
MaxStartups 4                                                    ## Specifies the maximum number of concurrent unauthenticated connections to the SSH daemon                                 # NUMBER_INT
ModuliFile /etc/ssh/moduli                                       ## Specifies the moduli file that contains the Diffie-Hellman groups                                                        # Absolute path to file
PasswordAuthentication no                                        ## Specifies whether password authentication is allowed                                                                     # yes | no
PermitEmptyPasswords no                                          ## Specifies whether the server allows login to accounts with empty password strings                                        # yes | no
PermitListen none                                                ## Specifies the addresses/ports on which a remote TCP port forwarding may listen                                           # PORT | HOST:PORT | any | none
PermitOpen none                                                  ## Specifies the destinations to which TCP port forwarding is permitted                                                     # HOST:PORT | any | none
PermitRootLogin no                                               ## Specifies whether root can log in using ssh                                                                              # yes | no | prohibit-password | forced-commands-only
#PermitTTY yes                                                   ## Specifies whether pty allocation is permitted                                                                            # yes | no
PermitTunnel no                                                  ## Specifies whether tunnel device forwarding is allowed                                                                    # yes | no | point-to-point | ethernet
PermitUserEnvironment no                                         ## Specifies whether ~/.ssh/environment and environment= options in ~/.ssh/authorized_keys are processed by sshd            # yes | no | PATTERN
PermitUserRC yes                                                 ## Specifies whether any ~/.ssh/rc file is executed                                                                         # yes | no
PerSourceMaxStartups none                                        ## Specifies the number of unauthenticated connections allowed from a given source address                                  # none | NUMBER_INT
#PerSourceNetBlockSize                                           ## Specifies the number of bits of source address that are grouped together for the purposes of applying limits             #
PidFile none                                                     ## Specifies the file that contains the process ID of the SSH daemon                                                        # none | Absolute path to file
Port 5822                                                        ## [MCD] Specifies the port number that sshd listens on                                                                     # NUMBER_INT
PrintLastLog yes                                                 ## Specifies whether sshd should print the date and time of the last user login when a user logs in interactively           # yes | no
PrintMotd yes                                                    ## Specifies whether sshd should print /etc/motd when a user logs in interactively                                          # yes | no
Protocol 2                                                       ## Specifies ssh protocol version                                                                                           # 2
PubkeyAuthentication yes                                         ## Specifies whether public key authentication is allowed                                                                   # yes | no
PubkeyAuthOptions none                                           ## Sets one or more public key authentication options                                                                       # none | touch-required | verify-required
RekeyLimit default none                                          ## Specifies the maximum amount of data that may be transmitted before the session key is renegotiated                      #
#RevokedKeys none                                                ## Specifies revoked public keys file                                                                                       # none | Absolute path to file
#RDomain                                                         ## Specifies an explicit routing domain that is applied after authentication has completed                                  #
#SecurityKeyProvider                                             ## Specifies a path to a library that will be used when loading FIDO authenticator-hosted keys                              # Absolute path to file
#SetEnv                                                          ## Specifies one or more environment variables to set in child sessions started by sshd                                     # NAME=VALUE
#StreamLocalBindMask                                             ## Sets the octal file creation mode mask (umask) used when creating a Unix-domain socket file for port forwarding          #
#StreamLocalBindUnlink no                                        ## Specifies whether to remove an existing Unix-domain socket file for port forwarding before creating a new one            # yes | no
StrictModes yes                                                  ## Specifies whether sshd should check file modes and ownership of the user's files before accepting login                  # yes | no
Subsystem sftp /usr/lib/ssh/sftp-server                          ## Configures an external subsystem                                                                                         # sftp-server | internal-sftp
SyslogFacility AUTH                                              ## Gives the facility code that is used when logging messages from sshd                                                     # DAEMON | USER | AUTH | LOCAL0 | LOCAL1 | LOCAL2 | LOCAL3 | LOCAL4 | LOCAL5 | LOCAL6 | LOCAL7
TCPKeepAlive no                                                  ## Specifies whether the system should send TCP keepalive messages to the other side                                        # yes | no
#TrustedUserCAKeys                                               ## Specifies a file containing public keys of certificate authorities that are trusted                                      # Absolute path to file
UseDNS no                                                        ## Specifies whether sshd should check that the resolved host name maps back to the very same IP address                    # yes | no
UsePAM no                                                        ## Enables the Pluggable Authentication Module interface                                                                    # yes | no
VersionAddendum none                                             ## Optionally specifies additional text to append to the SSH protocol banner                                                # none | TEXT
#X11DisplayOffset 10                                             ## Specifies the first display number available for sshd's X11 forwarding                                                   # NUMBER_INT
X11Forwarding no                                                 ## Specifies whether X11 forwarding is permitted                                                                            # yes | no
#X11UseLocalhost yes                                             ## Specifies whether sshd should bind the X11 forwarding server to the loopback address or to the wildcard address          # yes | no
#XAuthLocation /usr/bin/xauth                                    ## Specifies the full pathname of the xauth program                                                                         # none | Absolute path to file


## [MCD] Specifies the signature algorithms that will be accepted for hostbased authentication
# ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com,ecdsa-sha2-nistp521,ecdsa-sha2-nistp521-cert-v01@openssh.com,
# ecdsa-sha2-nistp384,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp256,ecdsa-sha2-nistp256-cert-v01@openssh.com,sk-ecdsa-sha2-nistp256@openssh.com,
# sk-ecdsa-sha2-nistp256-cert-v01@openssh.com,rsa-sha2-512,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa,ssh-rsa-cert-v01@openssh.com
HostbasedAcceptedAlgorithms ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com

## [MCD] Specifies the host key signature algorithms that the server offers
# ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com,ecdsa-sha2-nistp521,ecdsa-sha2-nistp521-cert-v01@openssh.com,
# ecdsa-sha2-nistp384,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp256,ecdsa-sha2-nistp256-cert-v01@openssh.com,sk-ecdsa-sha2-nistp256@openssh.com,
# sk-ecdsa-sha2-nistp256-cert-v01@openssh.com,rsa-sha2-512,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa,ssh-rsa-cert-v01@openssh.com
HostKeyAlgorithms ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com

## [MCD] Specifies the available KEX (Key Exchange) algorithms
# curve25519-sha256,curve25519-sha256@libssh.org,ecdh-sha2-nistp521,ecdh-sha2-nistp384,ecdh-sha2-nistp256,diffie-hellman-group-exchange-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,
# diffie-hellman-group14-sha256,diffie-hellman-group-exchange-sha1,diffie-hellman-group14-sha1,diffie-hellman-group1-sha1,sntrup761x25519-sha512@openssh.com
KexAlgorithms curve25519-sha256,curve25519-sha256@libssh.org

## [MCD] Specifies the available MAC (message authentication code) algorithms
# umac-128-etm@openssh.com,umac-64-etm@openssh.com,umac-128@openssh.com,umac-64@openssh.com,hmac-sha2-512-etm@openssh.com,hmac-sha2-256-etm@openssh.com,hmac-sha2-512,hmac-sha2-256,
# hmac-sha1-96-etm@openssh.com,hmac-sha1-etm@openssh.com,hmac-sha1-96,hmac-sha1,hmac-md5-96-etm@openssh.com,hmac-md5-etm@openssh.com,hmac-md5-96,hmac-md5
MACs umac-128-etm@openssh.com,hmac-sha2-512-etm@openssh.com

## [MCD] Specifies the signature algorithms that will be accepted for public key authentication
# ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com,ecdsa-sha2-nistp521,ecdsa-sha2-nistp521-cert-v01@openssh.com,
# ecdsa-sha2-nistp384,ecdsa-sha2-nistp384-cert-v01@openssh.com,ecdsa-sha2-nistp256,ecdsa-sha2-nistp256-cert-v01@openssh.com,sk-ecdsa-sha2-nistp256@openssh.com,
# sk-ecdsa-sha2-nistp256-cert-v01@openssh.com,rsa-sha2-512,rsa-sha2-512-cert-v01@openssh.com,rsa-sha2-256,rsa-sha2-256-cert-v01@openssh.com,ssh-rsa,ssh-rsa-cert-v01@openssh.com,
PubkeyAcceptedAlgorithms ssh-ed25519,ssh-ed25519-cert-v01@openssh.com,sk-ssh-ed25519@openssh.com,sk-ssh-ed25519-cert-v01@openssh.com
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;
