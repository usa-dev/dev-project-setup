### [RETURN TO README](README.md#markdown-header-table-of-contents)
---

 &nbsp; &nbsp;




### **Table of Contents**
- [LetsEncrypt Certbot](#markdown-header-letsencrypt-certbot) | *LetsEncrypt Certbot Generate & Verify SSL Certificate*
- [Nginx Install](#markdown-header-nginx-install) | *Nginx Install and Configure*
- [Nginx Simple Site Config](#markdown-header-nginx-simple-site-config) | *Setup nginx site config file*
- [Nginx Multi Sub-Domain Site Config](#markdown-header-nginx-multi-sub-domain-site-config) | *Setup nginx site config file*

 &nbsp; &nbsp;




### **LetsEncrypt Certbot**
*LetsEncrypt Certbot Generate & Verify SSL Certificate*
```sh
certbot certonly --manual --preferred-challenges dns -d example.com -d *.example.com
certbot certonly --manual --preferred-challenges dns --manual-auth-hook ~/porkbun-dns-auth.sh --manual-cleanup-hook ~/porkbun-dns-clean.sh -d example.com -d *.example.com -d *.sub.example.com
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Nginx Install**
*Nginx Install and Configure*
```sh

```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Nginx Simple Site Config**
*Setup nginx site config file*
```sh
sudo tee /etc/nginx/sites.d/example.com > /dev/null <<EOF
server {
    listen                  80;
    server_name             *.example.com;
    location /.well-known/acme-challenge/ {
        root                /var/www;
    }
    return 301 https://$host$request_uri;
}

server {
    listen                  443;
    server_name             *.example.com;
    root                    /var/www/example.com;
    ssl                     on;
    ssl_certificate         /etc/letsencrypt/live/example.com/cert.pem;
    ssl_certificate_key     /etc/letsencrypt/live/example.com/privkey.pem;
    expires                 15d;
}

EOF
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Nginx Multi Sub-Domain Site Config**
*Setup nginx site config file*
```sh
sudo tee > /etc/nginx/sites.d/example.com <<EOF
server {
    listen                  80;
    server_name             *.example.com;
    location /.well-known/acme-challenge/ {
        root                /var/www;
    }
    return 301 https://$host$request_uri;
}

server {
    listen                  443;
    server_name             "~^(?<sub>.+)\.example\.com$";
    root                    /var/www/example.com/$sub;
    index                   index.html index.htm;

    ssl                     on;
    ssl_certificate         /etc/letsencrypt/live/example.com/cert.pem;
    ssl_certificate_key     /etc/letsencrypt/live/example.com/privkey.pem;
    expires                 15d;

    error_page              404 = @homepage;

    location / {
        try_files           $uri $uri/ @homepage;
    }

    location @homepage {
        if ($uri = "/") {
            rewrite ^ $scheme://www.example.com;
        }
        return 302 / ;
    }
}
EOF
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;
