### [RETURN TO README](README.md#markdown-header-table-of-contents)
---

 &nbsp; &nbsp;




### **Table of Contents**
- [Description](#markdown-header-description) | *Notes about code and required fixes*
- [Markdown Links](#markdown-header-markdown-links) | *Links to markdown resources*
- [Template Definition](#markdown-header-template-definition) | *README Full Code*

 &nbsp; &nbsp;




## **Description**
Below is the README markdown Template.

_You will need to replace all_ &nbsp; &nbsp; **" \\`\`` "** &nbsp; &nbsp; _with_ &nbsp; &nbsp; **" ``` "** &nbsp; &nbsp; _because I couldn't find a way to escape it in the code._

[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




## **Markdown Links**

| Label | Web Link |
| --- | --- |
| Bitbucket Syntax | https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html |
| Common Mark Spec | https://spec.commonmark.org/current/ |
| HTML Entities Reference | https://html.spec.whatwg.org/entities.json |

[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




## **Template Definition**

```md
### [RETURN TO README](README.md#markdown-header-table-of-contents)
---

 &nbsp; &nbsp;




# **Application Name**
Description about the application

 &nbsp; &nbsp;




## **Table of Contents**
- [Description](#markdown-header-application-name) | *About the Application*
- [Installation](#markdown-header-installation) | *Project Installation*
- [Configuration](#markdown-header-configuration) | *Project Configuration*
- [FAQ](#markdown-header-faq) | *Frequently Asked Questions*
- [Breaking Changes](#markdown-header-breaking-changes) | *Changes incompatible with previous version*
- [Changelog](#markdown-header-changelog) | *A log of Project Changes*
- [License](#markdown-header-license) | *Project License and Other Licenses*
- [Title](#markdown-header-title) | *Description*

 &nbsp; &nbsp;




## **Installation**
*Describe how to install dependencies or build the project.*

```sh

\```

[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




## **Configuration**
*Describe any configuration that is needed for the project.*

```sh

\```

[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




## **FAQ**
*List of Frequently Asked Questions or link to faq.*

```sh

\```

[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




## **Breaking Changes**
*List of changes that will break code on upgrade.*

```sh

\```

[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




## **Changelog**
*List of changes and or link to changelog.*

```sh

\```

[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




## **License**
*The license under which the project is released and references to other license used within the project*

```sh

\```

[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;
