# Development Project Setup
This repository contains detailed information on how to setup commonly used things in a new project.
This repo will also contain information on commonly asked questions.

 &nbsp; &nbsp;




## **Table of Contents**
- [Markdown Navigation](#markdown-header-markdown-navigation) | *Markdown Navigation List*
- [License](#markdown-header-license) | *Repo License*

 &nbsp; &nbsp;




## **Markdown Navigation**
| Page Link | Page Title | Description |
| --- | --- | --- |
| [See More](template.md#markdown-header-table-of-contents) | README Template | *All the settings and configurations for git and ssh* |
| [See More](arch-linux.md#markdown-header-table-of-contents) | Arch Linux Install | *Arch Linux Installation, Configurations, and Modifications* |
| [See More](bash.d/README.md#markdown-header-table-of-contents) | Bash Files | *All the settings and configurations for Bash Terminal* |
| [See More](fonts.d/README.md#markdown-header-table-of-contents) | Font Files | *Font Files and Configurations* |
| [See More](git-ssh.md#markdown-header-table-of-contents) | GIT & SSH Settings | *All the settings and configurations for git and ssh* |
| [See More](nginx.d/README.md#markdown-header-table-of-contents) | Nginx Files | *All the settings and configurations for Nginx Config Files* |
| [See More](vmware.d/README.md#markdown-header-table-of-contents) | VMware Files | *All the settings and configurations for VMware Files* |
| [See More](vscode.md#markdown-header-table-of-contents) | VSCode Settings | *All the settings and configurations for VSCode Code Editor* |
| [See More](certbot.d/README.md#markdown-header-table-of-contents) | Certbot & LetsEncrypt Settings | *All the settings and configurations for LetsEncrypt Certbot* |
|  |  |  |

 &nbsp; &nbsp;




## **License**
Copyright &copy; 2021 usa-dev@bitbucket

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

--- [THE MIT LICENSE](https://opensource.org/licenses/MIT) ---