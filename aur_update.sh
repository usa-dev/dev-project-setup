#!/bin/env bash

FORCE=0
EXTS=0

while [[ $# -gt 0 ]]; do
	case $1 in
		-f|--force)
			FORCE=1
			shift
			;;
		-e|--extensions)
			EXTS=1
			shift
			;;
		-h|--help)
			echo "$0 [options]";
			echo ""
			echo "Options"
			echo " -e | --extensions     Install Packages Related to System Extensions"
			echo " -f | --force          Force extensions to install even if same version"
			echo " -h | --help           Show help file"
			exit 0
			;;
	esac
done

echo "  --- System Update"
sudo pacman --needed --noconfirm -S archlinux-keyring gnome-keyring
sudo pacman --needed --noconfirm --overwrite "*" -Sy npm
sudo pacman -Syyu --noconfirm
sudo mkdir -p /opt/version/

echo ""

{
	PKG='brave-bin'
	echo "  --- ${PKG^^} INSTALL STARTED"
	git clone https://aur.archlinux.org/$PKG.git
	cd $PKG

	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
	VER=$(brave --version 2>/dev/null | awk '{printf $3}');
	VER="${VER#*.}"

	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
		makepkg -si --noconfirm;
		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
	else
		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
	fi;

	cd ../
	rm -rf $PKG
	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
}

{
	PKG='opera'
	echo "  --- ${PKG^^} INSTALL STARTED"
	git clone https://aur.archlinux.org/$PKG.git
	cd $PKG

	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
	VER=$(opera --version 2>/dev/null);

	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
		makepkg -si --noconfirm;
		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
	else
		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
	fi;

	cd ../
	rm -rf $PKG
	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
}

{
	PKG='vscodium-bin'
	echo "  --- ${PKG^^} INSTALL STARTED"
	git clone https://aur.archlinux.org/$PKG.git
	cd $PKG

	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1 | cut -d'.' -f1-3);
	VER=$(codium --version 2>/dev/null | head -n1);

	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
		makepkg -si --noconfirm;
		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
	else
		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
	fi;

	cd ../
	rm -rf $PKG
	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
}

# {
# 	PKG='visual-studio-code-bin'
# 	echo "  --- ${PKG^^} INSTALL STARTED"
# 	git clone https://aur.archlinux.org/$PKG.git
# 	cd $PKG
#
# 	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
# 	VER=$(code --version 2>/dev/null | head -n1);
#
# 	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
# 		makepkg -si --noconfirm;
#		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
# 		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
# 	else
# 		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
# 	fi;
#
# 	cd ../
# 	rm -rf $PKG
# 	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
# }

# {
# 	PKG='drawio-desktop-bin'
# 	echo "  --- ${PKG^^} INSTALL STARTED"
# 	git clone https://aur.archlinux.org/$PKG.git
# 	cd $PKG
#
# 	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
# 	VER=$(drawio --version 2>/dev/null | tail -n 1);
#
# 	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
# 		makepkg -si --noconfirm;
#		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
# 		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
# 	else
# 		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
# 	fi;
#
# 	cd ../
# 	rm -rf $PKG
# 	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
# }

# {
# 	PKG='wps-office'
# 	echo "  --- ${PKG^^} INSTALL STARTED"
# 	git clone https://aur.archlinux.org/$PKG.git
# 	cd $PKG
#
# 	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
# 	VER=$(cat /usr/lib/office6/cfgs/setup.cfg 2>/dev/null | grep Version= | head -n4 | tr "=" " " | awk '{ printf "."$2 }' | cut -c2-);
#
# 	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
# 		makepkg -si --noconfirm;
#		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
# 		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
# 	else
# 		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
# 	fi;
#
# 	cd ../
# 	rm -rf $PKG
# 	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
# }

# {
# 	PKG='freeoffice'
# 	echo "  --- ${PKG^^} INSTALL STARTED"
# 	git clone https://aur.archlinux.org/$PKG.git
# 	cd $PKG
#
# 	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
# 	VER=$(strings /usr/lib/freeoffice/textmaker 2>/dev/null | grep -E ^Revision | awk '{printf $2}');
#
# 	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
# 		makepkg -si --noconfirm;
#		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
# 		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
# 	else
# 		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
# 	fi;
#
# 	cd ../
# 	rm -rf $PKG
# 	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
# }

{
	PKG='mongodb-bin'
	echo "  --- ${PKG^^} INSTALL STARTED"
	git clone https://aur.archlinux.org/$PKG.git
	cd $PKG

	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
	VER=$(mongod --version 2>/dev/null | grep version | head -n1 | awk '{ printf $3}' | cut -c2-);

	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
		makepkg -si --noconfirm;
		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
	else
		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
	fi;

	cd ../
	rm -rf $PKG
	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
}

{
	PKG='mongosh-bin'
	echo "  --- ${PKG^^} INSTALL STARTED"
	git clone https://aur.archlinux.org/$PKG.git
	cd $PKG

	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
	VER=$(mongosh --version 2>/dev/null);

	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
		makepkg -si --noconfirm;
		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
	else
		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
	fi;

	cd ../
	rm -rf $PKG
	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
}

{
	PKG='mongodb-tools-bin'
	echo "  --- ${PKG^^} INSTALL STARTED"
	git clone https://aur.archlinux.org/$PKG.git
	cd $PKG

	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
	VER=$(mongoexport --version 2>/dev/null | grep version | head -n1 | awk '{ printf $3}');

	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
		makepkg -si --noconfirm;
		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
	else
		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
	fi;

	cd ../
	rm -rf $PKG
	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
}

{
	PKG='mongodb-compass'
	echo "  --- ${PKG^^} INSTALL STARTED"
	git clone https://aur.archlinux.org/$PKG.git
	cd $PKG

	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
	VER=$(mongodb-compass --version 2>/dev/null | head -n1 | awk '{ printf $3}');

	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
		makepkg -si --noconfirm;
		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
	else
		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
	fi;

	cd ../
	rm -rf $PKG
	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
}

{
	PKG='redisinsight'
	echo "  --- ${PKG^^} INSTALL STARTED"
	git clone https://aur.archlinux.org/$PKG.git
	cd $PKG

	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
	VER=$(echo -e "$(strings /opt/redisinsight/resources/app.asar)" | grep -i '"version": ".*"' | grep -o "[0-9]*\.[0-9]*\.[0-9]*");

	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
		sudo rm /usr/bin/redisinsight;
		makepkg -sid --noconfirm;
		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
	else
		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
	fi;

	cd ../
	rm -rf $PKG
	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
}

# {
# 	PKG='postman-bin'
# 	echo "  --- ${PKG^^} INSTALL STARTED"
# 	git clone https://aur.archlinux.org/$PKG.git
# 	cd $PKG
#
# 	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
# 	VER=$(cat /opt/postman/app/resources/app/package.json 2>/dev/null | jq -r '.version');
#
# 	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
# 		makepkg -si --noconfirm;
#		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
# 		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
# 	else
# 		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
# 	fi;
#
# 	cd ../
# 	rm -rf $PKG
# 	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
# }

{
	PKG='slack-desktop'
	echo "  --- ${PKG^^} INSTALL STARTED"
	git clone https://aur.archlinux.org/$PKG.git
	cd $PKG

	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
	VER=$(slack --version 2>/dev/null);

	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
		makepkg -si --noconfirm;
		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
	else
		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
	fi;

	cd ../
	rm -rf $PKG
	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
}

{
	PKG='k6-bin'
	echo "  --- ${PKG^^} INSTALL STARTED"
	git clone https://aur.archlinux.org/$PKG.git
	cd $PKG

	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
	VER=$(k6 version 2>/dev/null | awk '{ printf $2}' | cut -c2-);

	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
		makepkg -si --noconfirm;
		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
	else
		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
	fi;

	cd ../
	rm -rf $PKG
	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
}

# {
# 	PKG='authy'
# 	echo "  --- ${PKG^^} INSTALL STARTED"
# 	git clone https://aur.archlinux.org/$PKG.git
# 	cd $PKG
#
# 	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
# 	VER=$(cat /opt/$PKG/version 2>/dev/null);
#
# 	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
# 		makepkg -si --noconfirm;
# 		echo "${NEW//\"/}" | sudo tee /opt/$PKG/version;
# 		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
# 	else
# 		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
# 	fi;
#
# 	cd ../
# 	rm -rf $PKG
# 	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
# }

# {
# 	PKG='bluemail'
# 	echo "  --- ${PKG^^} INSTALL STARTED"
# 	git clone https://aur.archlinux.org/$PKG.git
# 	cd $PKG
#
# 	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
# 	VER=$(cat /opt/$PKG/version 2>/dev/null);
#
# 	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
# 		makepkg -si --noconfirm;
# 		echo "--disable-gpu-sandbox --no-sandbox" > ~/.config/bluemail-flags.conf;
# 		echo "${NEW//\"/}" | sudo tee /opt/$PKG/version;
# 		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
# 	else
# 		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
# 	fi;
#
# 	cd ../
# 	rm -rf $PKG
# 	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
# }

{
	PKG='libuvc'
	echo "  --- ${PKG^^} INSTALL STARTED"
	git clone https://aur.archlinux.org/$PKG.git
	cd $PKG

	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
	VER=$(cat /usr/include/libuvc/libuvc_config.h 2>/dev/null | grep 'LIBUVC_VERSION_STR' | awk '{printf $3}');

	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
		makepkg -si --noconfirm;
		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
	else
		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
	fi;

	cd ../
	rm -rf $PKG
	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
}

{
	PKG='webcamoid'
	echo "  --- ${PKG^^} INSTALL STARTED"
	git clone https://aur.archlinux.org/$PKG.git
	cd $PKG

	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
	VER=$(webcamoid --version 2>/dev/null | awk '{printf $2}');

	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
		makepkg -si --noconfirm;
		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
	else
		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
	fi;

	cd ../
	rm -rf $PKG
	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
}

{
	PKG='zoom'
	echo "  --- ${PKG^^} INSTALL STARTED"
	git clone https://aur.archlinux.org/$PKG.git
	cd $PKG

	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
	VER=$(cat /opt/zoom/version.txt 2>/dev/null | cut -d '.' -f1-3);

	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
		makepkg -si --noconfirm;
		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
	else
		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
	fi;

	cd ../
	rm -rf $PKG
	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
}

# {
# 	PKG='python-inputs'
# 	echo "  --- ${PKG^^} INSTALL STARTED"
# 	git clone https://aur.archlinux.org/$PKG.git
# 	cd $PKG
#
# 	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
# 	VER=$(pip list | grep inputs | awk '{printf $2}');
#
# 	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
# 		makepkg -si --noconfirm;
# 		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
# 		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
# 	else
# 		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
# 	fi;
#
# 	cd ../
# 	rm -rf $PKG
# 	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
# }

# {
# 	PKG='python-mock'
# 	echo "  --- ${PKG^^} INSTALL STARTED"
# 	git clone https://aur.archlinux.org/$PKG.git
# 	cd $PKG
#
# 	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
# 	VER=$(pip list | grep mock | awk '{printf $2}');
#
# 	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
# 		makepkg -si --noconfirm;
# 		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
# 		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
# 	else
# 		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
# 	fi;
#
# 	cd ../
# 	rm -rf $PKG
# 	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
# }

# {
# 	PKG='python-vdf-solstice'
# 	echo "  --- ${PKG^^} INSTALL STARTED"
# 	git clone https://aur.archlinux.org/$PKG.git
# 	cd $PKG
#
# 	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
# 	VER=$(pip list | grep steam | awk '{printf $2}');
#
# 	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
# 		makepkg -si --noconfirm;
# 		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
# 		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
# 	else
# 		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
# 	fi;
#
# 	cd ../
# 	rm -rf $PKG
# 	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
# }

# {
# 	PKG='python-steam-solstice'
# 	echo "  --- ${PKG^^} INSTALL STARTED"
# 	git clone https://aur.archlinux.org/$PKG.git
# 	cd $PKG
#
# 	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
# 	VER=$(pip list | grep steam | awk '{printf $2}');
#
# 	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
# 		makepkg -si --noconfirm;
# 		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
# 		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
# 	else
# 		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
# 	fi;
#
# 	cd ../
# 	rm -rf $PKG
# 	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
# }

# {
# 	PKG='protonup-qt'
# 	echo "  --- ${PKG^^} INSTALL STARTED"
# 	git clone https://aur.archlinux.org/$PKG.git
# 	cd $PKG
#
# 	NEW=$(cat PKGBUILD | grep pkgver= | head -n1 | tr "=" "\n" | tail -n1);
# 	VER=$(pip show protonup-qt | grep -i version | awk '{printf $2}');
#
# 	if [ "${VER//\"/}" != "${NEW//\"/}" ]; then
# 		makepkg -si --noconfirm;
# 		echo "${NEW//\"/}" | sudo tee /opt/version/$PKG;
# 		printf "  --- INSTALLED VERSION ${NEW//\"/}\n";
# 	else
# 		printf "  --- SKIPPED VERSION ${VER//\"/}\n";
# 	fi;
#
# 	cd ../
# 	rm -rf $PKG
# 	printf "  --- ${PKG^^} INSTALL COMPLETE\n\n\n"
# }

# echo "gnome-$(gnome-terminal --version | cut -d ' ' -f4)_sh-$(gnome-shell --version | cut -d ' ' -f3)_name-your-branch"
