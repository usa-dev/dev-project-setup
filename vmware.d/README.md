




## VMware specific Commands
```sh
# Mount All Shared Drives
mkdir -p /tmp/shares
vmhgfs-fuse .host:/ /tmp/shares -o subtype=vmhgfs-fuse,allow_other

# Mount A Shared Drive
mkdir -p /tmp/$DIRNAME
vmhgfs-fuse .host:/$DIRNAME /tmp/$DIRNAME -o subtype=vmhgfs-fuse,allow_other

# Copy root files
for f in $(find /tmp/shares/public -name root* -o -name all*); do cp $f ~/.${f#*.} done
```


## Install VMware Player
```sh
sh VMware-Player-Full-17.0.2-21581411.x86_64.bundle --eulas-agreed --console --required

# Choose where startup script goes
# /lib/systemd/scripts
# /etc/init.d

sudo cp vmware /lib/systemd/scripts
sudo rm -rf /etc/init.d


# Fix vmmon / vmnet issues
sudo mv /usr/lib/vmware/modules/source/vmmon.tar /usr/lib/vmware/modules/source/vmmon.tar.bck
sudo mv /usr/lib/vmware/modules/source/vmnet.tar /usr/lib/vmware/modules/source/vmnet.tar.bck

wget -O vmware-host-modules-w17.0.2.tar.gz https://github.com/mkubecek/vmware-host-modules/archive/refs/tags/w17.0.2.tar.gz
mkdir ./vmware-host-modules-w17.0.2
tar --strip-components=1 -C ./vmware-host-modules-w17.0.2 -xf vmware-host-modules-w17.0.2.tar.gz
cd vmware-host-modules-w17.0.2
if [ $(grep -c '#include <net/gso.h>' vmnet-only/bridge.c) -eq 0 ]; then
  sed -i '46 a#include <net/gso.h>' vmnet-only/bridge.c
fi
tar -cf vmmon.tar vmmon-only
tar -cf vmnet.tar vmnet-only
sudo cp -v vmmon.tar vmnet.tar /usr/lib/vmware/modules/source/

# Look into
# strip --strip-debug

# Run any time vmware gives an error about starting vms
sudo /usr/bin/vmware-modconfig --console --install-all
```


## Install OVFTool
```sh
sudo tar -xf vmware-ovftool.tar.xz
sudo mv ./vmware-ovftool /usr/lib
sudo chmod +x /usr/lib/vmware-ovftool/ovftool /usr/lib/vmware-ovftool/ovftool.bin
sudo ln -s  /usr/lib/vmware-ovftool/ovftool /usr/bin/ovftool
```


## Export OVA Image
```sh
# ovftool --help examples
VMNAME="archlinux-amd64-2022.07.01"
VMPATH="/home/$USER/vmware/${VMNAME}"
ovftool --acceptAllEulas --noSSLVerify --disableVerification --X:skipContentLength $VMPATH/$VMNAME.vmx ~/vmware/$VMNAME.ovf
ovftool --acceptAllEulas --noSSLVerify --disableVerification --X:skipContentLength --compress=9 --targetType=OVA ~/vmware/$VMNAME.ovf ~/vmware/$VMNAME.ova
```


## Convert OVA to AMI
```sh
## Create Policy
tee -a trust-policy.json > /dev null <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": ["s3:ListBucket", "s3:GetBucketLocation"],
      "Resource": ["arn:aws:s3:::${AWS_BUCKET_NAME}"]
    },
    {
      "Effect": "Allow",
      "Action": ["s3:GetObject"],
      "Resource": ["arn:aws:s3:::${AWS_BUCKET_NAME}/*"]
    },
    {
      "Effect": "Allow",
      "Action": ["ec2:ModifySnapshotAttribute", "ec2:CopySnapshot", "ec2:RegisterImage", "ec2:Describe*"],
      "Resource": "*"
    }
  ]
}
EOF


## Setup AWS Role
Name=vmimport
Description=vmimport


## Trust Relationship
tee -a trust-rel.json > /dev null <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "vmie.amazonaws.com"
      },
      "Action": "sts:AssumeRole",
      "Condition": {
        "StringEquals": {
          "sts:ExternalId": "vmimport"
        }
      }
    }
  ]
}
EOF

## Attach Policy to Role


## Convert to AMI -- Option #1
aws ec2 import-image --license-type 'BYOL' --description 'Arch Linux amd64 2022.07.01' --disk-containers Format=OVA,Url='s3://aws-ami-images/archlinux-amd64-2022.07.01.ova'


## Convert to AMI -- Option #2
tee -a ~/vmware/container.json > /dev/null <<EOF
[
  {
    "Description": "Arch Linux amd64 2022.07.01",
    "Format": "ova",
    "UserBucket": {
      "S3Bucket": "aws-ami-images",
      "S3Key": "archlinux-amd64-2022.07.01.ova"
    }
  }
]
EOF
aws ec2 import-image --license-type "BYOL" --disk-containers "file://${HOME}/vmware/container.json"


## Get Status Update
aws ec2 describe-import-image-tasks --import-task-ids import-ami-XXXXXXXXXXX


## Information Reference
## Steps: https://docs.aws.amazon.com/vm-import/latest/userguide/vmimport-image-import.html
## Command: https://docs.aws.amazon.com/cli/latest/reference/ec2/import-image.html
## Article: https://medium.com/@eng.mohamed.m.saeed/convert-a-vm-to-ami-in-aws-b101a1c52e0e
## Custom: https://wiki.archlinux.org/title/Arch_Linux_AMIs_for_Amazon_Web_Services#Building_Arch_AMIs

```



## Install VMware Tools
```sh
pacman -S open-vm-tools gtkmm3
systemctl enable vmtoolsd
systemctl start vmtoolsd
systemctl enable vmware-vmblock-fuse
systemctl start vmware-vmblock-fuse
```