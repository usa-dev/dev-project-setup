#!/bin/env bash



## Set the paths to ignore when looking for files
## Paths are space delimited
## IGNOREPATHS="*/node_modules/* */logs/* */docs/* */build/*"
IGNOREPATHS="*/node_modules/* */build/* */logs/*"
PRETTIER_PRINTWIDTH=300
PRETTIER_INDENT=4
PRETTIER_TAB=4


## Create ignore path variable
unset FINDNOT
for path in $IGNOREPATHS; do FINDNOT="${FINDNOT:+$FINDNOT }-not -path $path"; done;



## Find and format all JS files
for file in $(find ./ -name '*.js' $FINDNOT); do
	uglifyjs $file --comments all -o $file
	prettier --bracket-same-line --indent $PRETTIER_INDENT --print-width $PRETTIER_PRINTWIDTH --tab-width $PRETTIER_TAB --write $file
	sed -i ':a;N;$!ba;s/\nfunction /\n\nfunction /g' $file
	sed -i ':a;N;$!ba;s/\nclass /\n\nclass /g' $file
	sed -i ':a;N;$!ba;s/\n}\n/\n}\n\n/g' $file
	sed -i ':a;N;$!ba;s/\n\n\+/\n\n/g' $file
done;



## Find and format all JS Modules
for file in $(find ./ -name '*.mjs' $FINDNOT); do
	uglifyjs $1 --module --comments all -o $1
	prettier --print-width 300
done;



## Find and format all JSON files
for file in $(find ./ -name '*.json' $FINDNOT); do
	uglifyjs $1 -o $1
	prettier --print-width 300
done;



## Find and format all JSON5 files
for file in $(find ./ -name '*.json5' $FINDNOT); do
	uglifyjs $1 --comments all -o $1
	prettier --print-width 300
done;



## Find and format all SCSS files
for file in $(find ./ -name '*.scss' $FINDNOT); do
	sassc -m -t compressed $file ${file%.*}.min.css
	sassc -m -t expanded $file ${file%.*}.css
done;



## Find and format all CSS files
for file in $(find ./ -name '*.css' $FINDNOT); do
	uglifyjs $1 --comments all -o $1
	prettier --print-width 300
done;



find ./ -name 'server.js' -not -path '*/node_modules/*' -exec bash -c 'uglifyjs $1 --comments all -o $1' - {} \;