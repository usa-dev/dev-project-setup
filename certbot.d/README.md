### [RETURN TO README](README.md#markdown-header-table-of-contents)
---

 &nbsp; &nbsp;




### **Table of Contents**
- [Certbot Setup](#markdown-header-certbot-setup) | *Configure Certbot Scripts*
- [LetsEncrypt Certbot Commands](#markdown-header-letsencrypt-certbot-commands) | *LetsEncrypt Certbot Generate & Verify SSL Certificate*

 &nbsp; &nbsp;




### **Certbot Setup**
*Configure Certbot Scripts*
```sh
sudo mkdir -p /etc/porkbun /usr/local/certbot


cat | sudo tee /etc/porkbun/credentials > /dev/null <<EOF
porkbun_secret_key=XXXXXXXXXXXXXXXXXXXXXX
porkbun_access_key=XXXXXXXXXXXXXXXXXXXXXX
EOF


cat | sudo tee /usr/local/certbot/porkbun-dns-auth.sh > /dev/null <<EOF
#!/bin/env sh

source /etc/porkbun/credentials 2> /dev/null
source \${HOME}/.config/porkbun/credentials 2> /dev/null
source \${HOME}/.porkbun/credentials 2> /dev/null
source \$PORKBUN_CREDENTIALS_FILE 2> /dev/null

wget -q -O- --method POST --header 'Content-Type:application/json' --body-data "{\\"secretapikey\\":\\"\${porkbun_secret_key}\\",\\"apikey\\":\\"\${porkbun_access_key}\\",\\"name\\":\\"_acme-challenge\\",\\"type\\":\\"TXT\\",\\"content\\":\\"\${CERTBOT_VALIDATION}\\",\\"ttl\\":\\"600\\"}" "https://porkbun.com/api/json/v3/dns/create/\${CERTBOT_DOMAIN}"

sleep 15

printf "\$(date '+%F %T') | \${CERTBOT_DOMAIN} | \${CERTBOT_REMAINING_CHALLENGES} | \${CERTBOT_VALIDATION}\n" >> /usr/local/var/log/dns-auth.log
EOF


cat | sudo tee /usr/local/certbot/porkbun-dns-clean.sh > /dev/null <<EOF
#!/bin/env sh

source /etc/porkbun/credentials 2> /dev/null
source \${HOME}/.config/porkbun/credentials 2> /dev/null
source \${HOME}/.porkbun/credentials 2> /dev/null
source \$PORKBUN_CREDENTIALS_FILE 2> /dev/null

DATA=\$(wget -q -O- --method POST --header='Content-Type:application/json' --body-data "{\\"secretapikey\\":\\"\${porkbun_secret_key}\\",\\"apikey\\":\\"\${porkbun_access_key}\\"}" "https://porkbun.com/api/json/v3/dns/retrieveByNameType/\${CERTBOT_DOMAIN}/TXT/_acme-challenge")
IDS=\$(echo \$DATA | jq -r .records[].id)

for ID in \$IDS; do
	wget -q -O- --method POST --header='Content-Type:application/json' --body-data "{\\"secretapikey\\":\\"\${porkbun_secret_key}\\",\\"apikey\\":\\"\${porkbun_access_key}\\"}" "https://porkbun.com/api/json/v3/dns/delete/\${CERTBOT_DOMAIN}/\${ID}"
	sleep 2
done

sleep 5

printf "\$(date '+%F %T') | \${CERTBOT_DOMAIN} | \${IDS}\n" >> /usr/local/var/log/dns-clean.log
EOF
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;





### **LetsEncrypt Certbot Commands**
*LetsEncrypt Certbot Generate & Verify SSL Certificate*
```sh
certbot certonly --manual --preferred-challenges dns -d example.com -d *.example.com
certbot certonly --manual --preferred-challenges dns --manual-auth-hook ~/porkbun-dns-auth.sh --manual-cleanup-hook ~/porkbun-dns-clean.sh -d example.com -d *.example.com -d *.sub.example.com
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;
