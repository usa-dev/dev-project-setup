#!/bin/env sh

if [ -f "${PORKBUN_CREDENTIALS_FILE:-$HOME}" ]; then
	source $PORKBUN_CREDENTIALS_FILE 2> /dev/null;
elif [ -f ${HOME}/.porkbun/credentials ]; then
	source ${HOME}/.porkbun/credentials 2> /dev/null;
elif [ -f  ${HOME}/.config/porkbun/credentials ]; then
	source ${HOME}/.config/porkbun/credentials 2> /dev/null;
elif [ -f /etc/porkbun/credentials ]; then
	source /etc/porkbun/credentials 2> /dev/null;
fi

DATA=$(wget -q -O- --method POST --header='Content-Type:application/json' --body-data "{\"secretapikey\":\"${porkbun_secret_key}\",\"apikey\":\"${porkbun_access_key}\"}" "https://porkbun.com/api/json/v3/dns/retrieveByNameType/${CERTBOT_DOMAIN}/TXT/_acme-challenge")
IDS=$(echo $DATA | jq -r .records[].id)

for ID in $IDS; do
	wget -q -O- --method POST --header='Content-Type:application/json' --body-data "{\"secretapikey\":\"${porkbun_secret_key}\",\"apikey\":\"${porkbun_access_key}\"}" "https://porkbun.com/api/json/v3/dns/delete/${CERTBOT_DOMAIN}/${ID}"
	sleep 2
done

sleep 5

printf "$(date '+%F %T') | ${CERTBOT_DOMAIN} | ${IDS}\n" >> /usr/local/var/log/dns-clean.log