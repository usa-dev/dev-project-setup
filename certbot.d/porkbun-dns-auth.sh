#!/bin/env sh

if [ -f "${PORKBUN_CREDENTIALS_FILE:-$HOME}" ]; then
	source $PORKBUN_CREDENTIALS_FILE 2> /dev/null;
elif [ -f ${HOME}/.porkbun/credentials ]; then
	source ${HOME}/.porkbun/credentials 2> /dev/null;
elif [ -f  ${HOME}/.config/porkbun/credentials ]; then
	source ${HOME}/.config/porkbun/credentials 2> /dev/null;
elif [ -f /etc/porkbun/credentials ]; then
	source /etc/porkbun/credentials 2> /dev/null;
fi

wget -q -O- --method POST --header 'Content-Type:application/json' --body-data "{\"secretapikey\":\"${porkbun_secret_key}\",\"apikey\":\"${porkbun_access_key}\",\"name\":\"_acme-challenge\",\"type\":\"TXT\",\"content\":\"${CERTBOT_VALIDATION}\",\"ttl\":\"600\"}" "https://porkbun.com/api/json/v3/dns/create/${CERTBOT_DOMAIN}"

sleep 15

printf "$(date '+%F %T') | ${CERTBOT_DOMAIN} | ${CERTBOT_REMAINING_CHALLENGES} | ${CERTBOT_VALIDATION}\n" >> /usr/local/var/log/dns-auth.log