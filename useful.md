### [RETURN TO README](README.md#markdown-header-table-of-contents)
---

 &nbsp; &nbsp;




### **Table of Contents**
- [Copy All Bash Files](#markdown-header-copy-all-bash-files) | *Terminal Settings*
- [Title](#markdown-header-title) | *Description*
- [Title](#markdown-header-title) | *Description*
- [Title](#markdown-header-title) | *Description*
- [Title](#markdown-header-title) | *Description*
- [Title](#markdown-header-title) | *Description*

 &nbsp; &nbsp;




### **Copy All Bash Files**

*Bash commands that are used by users*

```sh
# Copy Root Bash Files
find /tmp/shares/public/bash.d \( -name all.* -o -name root.* \) -exec bash -c 'F=$(basename $1); cp $1 ~/.${F#*.}' -- {} \;

# Copy User Bash Files
find /tmp/shares/public/bash.d \( -name all.* -o -name user.* \) -exec bash -c 'F=$(basename $1); cp $1 ~/.${F#*.}' -- {} \;
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Bluetooth**

*Bash commands that are used by users*

```sh
bluetoothctl list						# List Agents
bluetoothctl power on					# Turn on controller
bluetoothctl discoverable on			# Turn on discover
bluetoothctl pairable on				# Turn on pair
bluetoothctl scan on					# Scan for devices
bluetoothctl devices					# List devices
bluetoothctl pair 00:00:00				# Pair a device
bluetoothctl connect 00:00:00			# Connect to device
bluetoothctl disconnect 00:00:00		# Disconnect a device
bluetoothctl remove 00:00:00			# Remove a device
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **ColorPicker**

*Color picker when system picker doesn't work*

```sh
sudo tee /usr/bin/colorpicker >/dev/null <<EOF
#!/usr/bin/env bash

colors=$(gdbus call --session --dest org.gnome.Shell.Screenshot --object-path /org/gnome/Shell/Screenshot --method org.gnome.Shell.Screenshot.PickColor | grep -o "[0-9\.]*" | xargs -n3 sh -c 'printf "%.0f %.0f %.0f" $(echo "$0 * 255" | bc) $(echo "$1 * 255" | bc) $(echo "$2 * 255" | bc)');

printf "RGB: %d %d %d\n" $colors
printf "HEX: #%02x%02x%02x\n" $colors
EOF
sudo chmod 755 /usr/bin/colorpicker
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Resizeable Input**


```sh
## Notes
## -------------------------------
## input => record | remark
## select => dropdown
## textarea => note | ledger
```

**HTML**
```html
<input type="text" oninput="inputresize(this),capnext(this)">
```

**CSS**
```css
:root{
	--input-width: 4ch;
}

input {
	width: 0;
	min-width: var(--input-width);
}
```

**JS**
```js
function inputresize( input ){
	const len = `${input.value||''}`.length;
	if ( len > 4 ) input.style.setProperty('--input-width', `${len}ch`);
	else input.style.removeProperty('--input-width');
}
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Random Notes**


```sh
## Disable funding message
npm config set fund false
```
