### [RETURN TO README](README.md#markdown-header-table-of-contents)
---

 &nbsp; &nbsp;




### **Table of Contents**
- [Title](#markdown-header-title) | *Description*
- [Title](#markdown-header-title) | *Description*
- [Title](#markdown-header-title) | *Description*
- [Links](#markdown-header-links) | *More Information and Resources*

 &nbsp; &nbsp;




### **Title**

*Description*

```sh
#!/usr/bin/env bash

## Download Default Font
pacman -S --noconfirm tamsyn-font
setfont /usr/share/kbd/consolefonts/Tamsyn8x16r.psf.gz
sudo cat > /etc/vconsole.conf <<EOF
FONT=Tamsyn8x16r
EOF

## Make Directories
sudo mkdir -p /usr/local/share/fonts/{RobotoFlex,NotoSansMono,NotoSans,NotoSerif,RobotoMono,FiraCode,FiraMono,Roboto,NotoEmoji}

## Download Fonts From Github
sudo wget -4 -qO /usr/local/share/fonts/RobotoFlex/RobotoFlex-VF.ttf "https://raw.githubusercontent.com/googlefonts/roboto-flex/main/fonts/RobotoFlex%5BGRAD%2CXOPQ%2CXTRA%2CYOPQ%2CYTAS%2CYTDE%2CYTFI%2CYTLC%2CYTUC%2Copsz%2Cslnt%2Cwdth%2Cwght%5D.ttf"
sudo wget -4 -qO /usr/local/share/fonts/NotoSansMono/NotoSansMono-VF.ttf "https://raw.githubusercontent.com/notofonts/noto-fonts/main/unhinted/variable-ttf/NotoSansMono-VF.ttf"
sudo wget -4 -qO /usr/local/share/fonts/NotoSans/NotoSans-VF.ttf "https://raw.githubusercontent.com/notofonts/noto-fonts/main/unhinted/variable-ttf/NotoSans-VF.ttf"
sudo wget -4 -qO /usr/local/share/fonts/NotoSans/NotoSans-Italic-VF.ttf "https://raw.githubusercontent.com/notofonts/noto-fonts/main/unhinted/variable-ttf/NotoSans-Italic-VF.ttf"
sudo wget -4 -qO /usr/local/share/fonts/NotoSerif/NotoSerif-VF.ttf "https://raw.githubusercontent.com/notofonts/noto-fonts/main/unhinted/variable-ttf/NotoSerif-VF.ttf"
sudo wget -4 -qO /usr/local/share/fonts/NotoSerif/NotoSerif-Italic-VF.ttf "https://raw.githubusercontent.com/notofonts/noto-fonts/main/unhinted/variable-ttf/NotoSerif-Italic-VF.ttf"
sudo wget -4 -qO /usr/local/share/fonts/RobotoMono/RobotoMono-VF.ttf "https://raw.githubusercontent.com/googlefonts/RobotoMono/main/fonts/variable/RobotoMono%5Bwght%5D.ttf"
sudo wget -4 -qO /usr/local/share/fonts/RobotoMono/RobotoMono-Italic-VF.ttf "https://raw.githubusercontent.com/googlefonts/RobotoMono/main/fonts/variable/RobotoMono-Italic%5Bwght%5D.ttf"
sudo wget -4 -qO /usr/local/share/fonts/FiraCode/FiraCode-VF.ttf "https://unpkg.com/firacode/distr/variable_ttf/FiraCode-VF.ttf"

## Download Fonts From Google
wget -4 -qO- "https://fonts.google.com/download?family=Fira%20Mono" | sudo bsdtar -xf- --exclude *.txt --exclude static -C /usr/local/share/fonts/FiraMono
wget -4 -qO- "https://fonts.google.com/download?family=Roboto" | sudo bsdtar -xf- --exclude *.txt --exclude static -C /usr/local/share/fonts/Roboto
wget -4 -qO- "https://fonts.google.com/download?family=Noto%20Emoji" | sudo bsdtar -xf- --exclude *.txt --exclude static -C /usr/local/share/fonts/NotoEmoji

## Fix Ownership
chown -R root:root /usr/share/fonts /usr/local/share/fonts

## Update Font Cache
find /usr/local/share/fonts -mindepth 1 -type d -exec /usr/bin/bash -c 'test -e /usr/bin/mkfontscale && /usr/bin/mkfontscale $1; test -e /usr/bin/mkfontdir && /usr/bin/mkfontdir $1; fc-cache -r $1' -- {} \;
find /usr/share/fonts -mindepth 1 -type d -exec /usr/bin/bash -c 'test -e /usr/bin/mkfontscale && /usr/bin/mkfontscale $1; test -e /usr/bin/mkfontdir && /usr/bin/mkfontdir $1; fc-cache -r $1' -- {} \;
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Title**

*Description*

```sh
## Generate WOFF2 Fonts [ /usr/local/share/fonts ]
sudo find /usr/local/share/fonts -name *.ttf -exec woff2_compress {} \;
cd /usr/local/share/fonts
find -name *.woff2 -exec bash -c 'DIR=$(dirname $1 | cut -c3-); mkdir -p ~/fonts/$DIR && sudo mv $1 ~/fonts/$DIR' -- {} \;

## Generate WOFF2 Fonts [ /usr/share/fonts ]
sudo find /usr/share/fonts -name *.ttf -exec woff2_compress {} \;
cd /usr/share/fonts
find -name *.woff2 -exec bash -c 'DIR=$(dirname $1 | cut -c3-); mkdir -p ~/fonts/$DIR && sudo mv $1 ~/fonts/$DIR' -- {} \;
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




### **Title**

*Description*

```sh
## First, convert ttf to bdf:

otf2bdf -p 20 -r 96 -o font.bdf font.ttf

## Play with -p parameter to change glyph size. Then convert bdf to psf:

bdf2psf --fb font.bdf /usr/share/bdf2psf/standard.equivalents /usr/share/bdf2psf/fontsets/Uni2.512 512 font.psf

## You can use another fontset or even copy and modify an existing one to add glyphs for characters
## you want to see in TTY. For example, I like radioactive sign, so I've appended U+2666 to the font set file.
##
## If bdf2psf complains for 'width is not integer number', either change glyph size or just replace AVERAGE_WIDTH
## in the bdf file (it's a plain text) to something round. e.g. if it is 139, change it to 140 and so on.

## -----------------------------------

cd ~/Downloads
git clone https://aur.archlinux.org/otf2bdf.git
git clone https://aur.archlinux.org/bdf2psf.git
cd ~/Downloads/otf2bdf && makepkg -si
cd ~/Downloads/bdf2psf && makepkg -si

```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;




## **Links**

| Label | Web Link |
| --- | --- |
| PSF Tools | http://www.seasip.info/Unix/PSF/index.html |
| Reddit Post TTF2PSF | https://www.reddit.com/r/linuxquestions/comments/7st7hz/any_way_to_convert_ttf_files_to_psf_files/ |

[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;
