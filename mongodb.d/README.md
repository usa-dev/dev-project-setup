### [RETURN TO README](README.md#markdown-header-table-of-contents)
---

 &nbsp; &nbsp;




### **Table of Contents**
- [Disable THP](#markdown-header-disable-thp) | *Disable Transparent Huge Pages*

 &nbsp; &nbsp;




### **Disable THP**
*Disable Transparent Huge Pages*
```sh
cat | sudo tee /etc/systemd/system/disable-transparent-huge-pages.service >/dev/null <<EOF
[Unit]
Description=Disable Transparent Huge Pages (THP)
DefaultDependencies=no
After=sysinit.target local-fs.target
Before=mongodb.service

[Service]
Type=oneshot
ExecStart=/bin/sh -c 'echo never | tee /sys/kernel/mm/transparent_hugepage/enabled > /dev/null'

[Install]
WantedBy=basic.target
EOF
```
[Return to TOC](#markdown-header-table-of-contents)

 &nbsp; &nbsp;
